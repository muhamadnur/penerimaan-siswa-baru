-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 22 Jun 2020 pada 09.55
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `penerimaan_siswa`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_ayah_kandung`
--

CREATE TABLE `data_ayah_kandung` (
  `id` int(11) NOT NULL,
  `nama_ayah` varchar(50) NOT NULL,
  `nik_ayah` varchar(25) NOT NULL,
  `tanggal_lahir_ayah` varchar(50) NOT NULL,
  `pendidikan_ayah` varchar(50) NOT NULL,
  `pekerjaan_ayah` varchar(50) NOT NULL,
  `penghasilan_ayah` varchar(50) NOT NULL,
  `berkebutuhan_khusus_ayah` varchar(50) NOT NULL,
  `siswa_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `data_ayah_kandung`
--

INSERT INTO `data_ayah_kandung` (`id`, `nama_ayah`, `nik_ayah`, `tanggal_lahir_ayah`, `pendidikan_ayah`, `pekerjaan_ayah`, `penghasilan_ayah`, `berkebutuhan_khusus_ayah`, `siswa_id`) VALUES
(15, 'Sunarto', '942304923', '2020-06-21', 'Tidak sekolah', 'Tidak bekerja', 'Kurang dari 500,000', 'Tidak', 'ID00000001'),
(16, 'Ahmad Supaidi', '923840293', '2020-06-14', 'Tidak sekolah', 'Tidak bekerja', 'Kurang dari 500,000', 'Tidak', 'ID00000003');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_beasiswa`
--

CREATE TABLE `data_beasiswa` (
  `id` int(11) NOT NULL,
  `jenis_beasiswa` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  `tahun_mulai` varchar(4) NOT NULL,
  `tahun_selesai` varchar(4) NOT NULL,
  `siswa_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `data_beasiswa`
--

INSERT INTO `data_beasiswa` (`id`, `jenis_beasiswa`, `keterangan`, `tahun_mulai`, `tahun_selesai`, `siswa_id`) VALUES
(18, '-', '-', '-', '-', 'ID00000001'),
(19, '-', '-', '-', '-', 'ID00000003');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_guru`
--

CREATE TABLE `data_guru` (
  `id_guru` varchar(10) NOT NULL,
  `nama_guru` varchar(128) NOT NULL,
  `email_guru` varchar(128) NOT NULL,
  `username` varchar(128) NOT NULL,
  `nomor_telephone` varchar(15) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `data_guru`
--

INSERT INTO `data_guru` (`id_guru`, `nama_guru`, `email_guru`, `username`, `nomor_telephone`, `alamat`) VALUES
('ID00000002', 'Muhamad Nur Sukur', 'nur@mail.com', 'nur', '392302', 'Jakarta Selatan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_ibu_kandung`
--

CREATE TABLE `data_ibu_kandung` (
  `id` int(11) NOT NULL,
  `nama_ibu` varchar(50) NOT NULL,
  `nik_ibu` varchar(25) NOT NULL,
  `tanggal_lahir_ibu` varchar(50) NOT NULL,
  `pendidikan_ibu` varchar(50) NOT NULL,
  `pekerjaan_ibu` varchar(50) NOT NULL,
  `penghasilan_ibu` varchar(50) NOT NULL,
  `berkebutuhan_khusus_ibu` varchar(50) NOT NULL,
  `siswa_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `data_ibu_kandung`
--

INSERT INTO `data_ibu_kandung` (`id`, `nama_ibu`, `nik_ibu`, `tanggal_lahir_ibu`, `pendidikan_ibu`, `pekerjaan_ibu`, `penghasilan_ibu`, `berkebutuhan_khusus_ibu`, `siswa_id`) VALUES
(16, 'Sunarti', '342393', '2020-06-22', 'Tidak sekolah', 'Tidak bekerja', 'Kurang dari 500,000', 'Tidak', 'ID00000001'),
(17, 'Sulastri', '23849023948', '2020-06-14', 'Tidak sekolah', 'Tidak bekerja', 'Kurang dari 500,000', 'Tidak', 'ID00000003');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_kontak`
--

CREATE TABLE `data_kontak` (
  `id` int(11) NOT NULL,
  `telephone_rumah` varchar(15) NOT NULL,
  `nomor_hp` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `siswa_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `data_kontak`
--

INSERT INTO `data_kontak` (`id`, `telephone_rumah`, `nomor_hp`, `email`, `siswa_id`) VALUES
(12, '', '2327238', 'admin@mail.com', 'ID00000001'),
(13, '021832984239', '0857123612321', 'syahrul@mail.com', 'ID00000003');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_prestasi`
--

CREATE TABLE `data_prestasi` (
  `id` int(11) NOT NULL,
  `jenis_prestasi` varchar(50) NOT NULL,
  `tingkat_prestasi` varchar(50) NOT NULL,
  `nama_prestasi` varchar(128) NOT NULL,
  `tahun` varchar(4) NOT NULL,
  `penyelenggaraan` varchar(128) NOT NULL,
  `siswa_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `data_prestasi`
--

INSERT INTO `data_prestasi` (`id`, `jenis_prestasi`, `tingkat_prestasi`, `nama_prestasi`, `tahun`, `penyelenggaraan`, `siswa_id`) VALUES
(20, '-', '-', '-', '-', '-', 'ID00000001'),
(21, 'Sains', 'Sekolah', 'IPA', '2020', 'SMP N 1 Bogor', 'ID00000003');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_pribadi_siswa`
--

CREATE TABLE `data_pribadi_siswa` (
  `no_registrasi` varchar(128) NOT NULL,
  `tanggal_registrasi` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `nama_lengkap` varchar(128) NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `nisn` int(10) NOT NULL,
  `nik` varchar(20) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `nomor_akta_lahir` varchar(50) NOT NULL,
  `agama` varchar(50) NOT NULL,
  `kewarganegaraan` varchar(10) NOT NULL,
  `nama_negara` varchar(128) NOT NULL,
  `berkebutuhan_khusus` varchar(100) NOT NULL,
  `alamat_jalan` text NOT NULL,
  `rt` varchar(3) NOT NULL,
  `rw` varchar(3) NOT NULL,
  `nama_dusun` varchar(50) NOT NULL,
  `nama_desa` varchar(50) NOT NULL,
  `kecamatan` varchar(50) NOT NULL,
  `kode_pos` int(5) NOT NULL,
  `lintang` varchar(50) NOT NULL,
  `bujur` varchar(50) NOT NULL,
  `tempat_tinggal` varchar(50) NOT NULL,
  `moda_transportasi` varchar(50) NOT NULL,
  `nomor_kks` varchar(50) NOT NULL,
  `anak_keberapa` int(2) NOT NULL,
  `penerima_kps` varchar(10) NOT NULL,
  `nomor_kps` varchar(50) NOT NULL,
  `usulan_dari_sekolah` varchar(10) NOT NULL,
  `penerima_kip` varchar(10) NOT NULL,
  `nomor_kip` varchar(50) NOT NULL,
  `nama_kip` varchar(50) NOT NULL,
  `kartu_kip` varchar(50) NOT NULL,
  `alasan_layak_pip` varchar(50) NOT NULL,
  `bank` varchar(50) NOT NULL,
  `nomor_rekening` varchar(50) NOT NULL,
  `nama_rekening` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `data_pribadi_siswa`
--

INSERT INTO `data_pribadi_siswa` (`no_registrasi`, `tanggal_registrasi`, `nama_lengkap`, `jenis_kelamin`, `nisn`, `nik`, `tempat_lahir`, `tanggal_lahir`, `nomor_akta_lahir`, `agama`, `kewarganegaraan`, `nama_negara`, `berkebutuhan_khusus`, `alamat_jalan`, `rt`, `rw`, `nama_dusun`, `nama_desa`, `kecamatan`, `kode_pos`, `lintang`, `bujur`, `tempat_tinggal`, `moda_transportasi`, `nomor_kks`, `anak_keberapa`, `penerima_kps`, `nomor_kps`, `usulan_dari_sekolah`, `penerima_kip`, `nomor_kip`, `nama_kip`, `kartu_kip`, `alasan_layak_pip`, `bank`, `nomor_rekening`, `nama_rekening`) VALUES
('ID00000001', '2020-06-21 16:40:17', 'Administrator', 'Laki-laki', 9734, '293432', 'Jakarta', '2020-06-21', '234230', 'Islam', 'WNI', 'Indonesia', 'Tidak', 'Jalan Semanggi', '09', '23', '', 'Semanggi', 'Semanggi', 23451, '', '', 'Bersama orang tua', 'Jalan kaki', '0342493', 2, 'Tidak', '-', 'Tidak', 'Tidak', '-', '-', 'Tidak', '-', 'Mandiri', '124000919321', 'Administrato'),
('ID00000003', '2020-06-21 19:27:08', 'Muhamad Syahrul', 'Laki-laki', 3456, '324293482309', 'Bogor', '2020-06-01', '129402184912', 'Islam', 'WNI', 'Indonesia', 'Tidak', 'Jalan Raya Bogor', '03', '09', '', 'Pabuaran', 'Cibonong', 23476, '', '', 'Bersama orang tua', 'Jalan kaki', '', 2, 'Tidak', '-', 'Tidak', 'Tidak', '-', '-', 'Tidak', '-', 'Mandiri', '12400009876442', 'Muhamad Syahrul');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_priodik`
--

CREATE TABLE `data_priodik` (
  `id` int(11) NOT NULL,
  `tinggi_badan` int(11) NOT NULL,
  `berat_badan` int(11) NOT NULL,
  `jarak` varchar(25) NOT NULL,
  `km` varchar(10) NOT NULL,
  `waktu_tempuh` varchar(11) NOT NULL,
  `jumlah_saudara` int(11) NOT NULL,
  `siswa_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `data_priodik`
--

INSERT INTO `data_priodik` (`id`, `tinggi_badan`, `berat_badan`, `jarak`, `km`, `waktu_tempuh`, `jumlah_saudara`, `siswa_id`) VALUES
(19, 170, 65, 'Lebih dari 1 km', '4', '15', 2, 'ID00000001'),
(20, 165, 55, 'Lebih dari 1 km', '5', '30', 3, 'ID00000003');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_wali`
--

CREATE TABLE `data_wali` (
  `id` int(11) NOT NULL,
  `nama_wali` varchar(50) NOT NULL,
  `nik_wali` varchar(25) NOT NULL,
  `tanggal_lahir_wali` varchar(50) NOT NULL,
  `pendidikan_wali` varchar(50) NOT NULL,
  `pekerjaan_wali` varchar(50) NOT NULL,
  `penghasilan_wali` varchar(50) NOT NULL,
  `siswa_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `form_berkas`
--

CREATE TABLE `form_berkas` (
  `id` int(11) NOT NULL,
  `siswa_id` varchar(11) NOT NULL,
  `nama_siswa` varchar(128) NOT NULL,
  `gelombang` varchar(20) NOT NULL,
  `id_penerima` varchar(10) NOT NULL,
  `nama_penerima` varchar(256) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `form_berkas`
--

INSERT INTO `form_berkas` (`id`, `siswa_id`, `nama_siswa`, `gelombang`, `id_penerima`, `nama_penerima`, `created_at`) VALUES
(18, 'ID00000003', 'Muhamad Syahrul', 'Gelombang 3', 'ID00000001', 'Administrator', '2020-06-22 01:39:56'),
(19, 'ID00000001', 'Administrator', 'Gelombang 2', 'ID00000002', 'Muhamad Nur Sukur', '2020-06-22 01:43:23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_pembayaran`
--

CREATE TABLE `jenis_pembayaran` (
  `id` int(11) NOT NULL,
  `nama_pembayaran` varchar(50) NOT NULL,
  `jumlah` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `jenis_pembayaran`
--

INSERT INTO `jenis_pembayaran` (`id`, `nama_pembayaran`, `jumlah`) VALUES
(5, 'Sarana Gedung', '1000000'),
(6, 'MPLS', '500000'),
(7, 'Psikotes', '500000'),
(8, 'Sarana Praktek', '1000000'),
(9, 'Koperasi', '250000'),
(10, 'OSIS', '250000'),
(11, 'Kartu Pelajar', '50000'),
(12, 'Seragam', '1000000'),
(13, 'Pembayaran Daftar Ulang', '150000'),
(14, 'Uang Pengembangan', '1000000'),
(15, 'Pembayaran SPP', '300000'),
(16, 'Pendaftaran', '1000000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id` int(11) NOT NULL,
  `no_faktur` varchar(10) NOT NULL,
  `tanggal` varchar(10) NOT NULL,
  `siswa_id` varchar(10) NOT NULL,
  `nama_siswa` varchar(128) NOT NULL,
  `jenis_pemb` varchar(50) NOT NULL,
  `bayar` int(10) NOT NULL,
  `keterangan` varchar(128) NOT NULL,
  `user_id` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pembayaran`
--

INSERT INTO `pembayaran` (`id`, `no_faktur`, `tanggal`, `siswa_id`, `nama_siswa`, `jenis_pemb`, `bayar`, `keterangan`, `user_id`, `created_at`) VALUES
(35, 'IN00000001', '21-06-2020', 'ID00000008', 'Administrator', 'Koperasi', 250000, 'koperasi', 'ID00000001', '2020-06-21 21:31:08'),
(36, 'IN00000002', '22-06-2020', 'ID00000008', 'Administrator', 'OSIS', 250000, 'osis', 'ID00000001', '2020-06-21 17:06:53'),
(37, 'IN00000003', '22-06-2020', 'ID00000008', 'ahmad', 'Kartu Pelajar', 50000, '', 'ID00000001', '2020-06-21 17:33:45'),
(38, 'IN00000004', '22-06-2020', 'ID00000001', 'Administrator', 'Pendaftaran', 1000000, 'pendaftaran', 'ID00000001', '2020-06-21 18:27:06'),
(39, 'IN00000005', '22-06-2020', 'ID00000001', 'Administrator', 'Sarana Gedung', 1000000, 'gedung', 'ID00000002', '2020-06-21 18:27:55'),
(40, 'IN00000006', '22-06-2020', 'ID00000001', 'Administrator', 'Pembayaran SPP', 300000, '', 'ID00000002', '2020-06-21 18:36:30'),
(41, 'IN00000007', '22-06-2020', 'ID00000003', 'Muhamad Syahrul', 'Kartu Pelajar', 50000, 'Kartu pelajar', 'ID00000002', '2020-06-21 19:34:16'),
(42, 'IN00000007', '22-06-2020', 'ID00000003', 'Muhamad Syahrul', 'Koperasi', 250000, 'koperasi', 'ID00000002', '2020-06-21 19:34:16'),
(43, 'IN00000007', '22-06-2020', 'ID00000003', 'Muhamad Syahrul', 'MPLS', 500000, 'mpls', 'ID00000002', '2020-06-21 19:34:16'),
(44, 'IN00000007', '22-06-2020', 'ID00000003', 'Muhamad Syahrul', 'OSIS', 250000, 'osis', 'ID00000002', '2020-06-21 19:34:16'),
(45, 'IN00000007', '22-06-2020', 'ID00000003', 'Muhamad Syahrul', 'Pembayaran Daftar Ulang', 150000, 'daftar ulang', 'ID00000002', '2020-06-21 19:34:16'),
(46, 'IN00000007', '22-06-2020', 'ID00000003', 'Muhamad Syahrul', 'Pembayaran SPP', 300000, 'spp 1 bulan', 'ID00000002', '2020-06-21 19:34:16'),
(47, 'IN00000007', '22-06-2020', 'ID00000003', 'Muhamad Syahrul', 'Pendaftaran', 1000000, 'pendaftaran', 'ID00000002', '2020-06-21 19:34:16'),
(48, 'IN00000007', '22-06-2020', 'ID00000003', 'Muhamad Syahrul', 'Psikotes', 500000, 'psikotes', 'ID00000002', '2020-06-21 19:34:16'),
(49, 'IN00000007', '22-06-2020', 'ID00000003', 'Muhamad Syahrul', 'Sarana Gedung', 1000000, 'sarana gedung', 'ID00000002', '2020-06-21 19:34:16'),
(50, 'IN00000007', '22-06-2020', 'ID00000003', 'Muhamad Syahrul', 'Sarana Praktek', 1000000, 'sarana praktek', 'ID00000002', '2020-06-21 19:34:17'),
(51, 'IN00000007', '22-06-2020', 'ID00000003', 'Muhamad Syahrul', 'Seragam', 1000000, 'seragam', 'ID00000002', '2020-06-21 19:34:17'),
(52, 'IN00000007', '22-06-2020', 'ID00000003', 'Muhamad Syahrul', 'Uang Pengembangan', 1000000, 'uang pengembangan', 'ID00000002', '2020-06-21 19:34:17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendaftaran_keluar`
--

CREATE TABLE `pendaftaran_keluar` (
  `id` int(11) NOT NULL,
  `jenis_keluar` varchar(50) NOT NULL,
  `tanggan_keluar` varchar(10) NOT NULL,
  `alasan` text NOT NULL,
  `siswa_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pendaftaran_keluar`
--

INSERT INTO `pendaftaran_keluar` (`id`, `jenis_keluar`, `tanggan_keluar`, `alasan`, `siswa_id`) VALUES
(13, '-', '-', '-', 'ID00000001'),
(14, '-', '-', '-', 'ID00000003');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penyerahan_berkas`
--

CREATE TABLE `penyerahan_berkas` (
  `id` int(11) NOT NULL,
  `siswa_id` varchar(10) NOT NULL,
  `nama_siswa` varchar(128) NOT NULL,
  `gelombang` varchar(25) NOT NULL,
  `formulir` varchar(10) NOT NULL,
  `ijazah` varchar(10) NOT NULL,
  `skhun` varchar(10) NOT NULL,
  `raport` varchar(10) NOT NULL,
  `akta` varchar(10) NOT NULL,
  `kk` varchar(10) NOT NULL,
  `ktp` varchar(10) NOT NULL,
  `kis` varchar(10) NOT NULL,
  `pas_foto` varchar(10) NOT NULL,
  `lain_lain` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `penyerahan_berkas`
--

INSERT INTO `penyerahan_berkas` (`id`, `siswa_id`, `nama_siswa`, `gelombang`, `formulir`, `ijazah`, `skhun`, `raport`, `akta`, `kk`, `ktp`, `kis`, `pas_foto`, `lain_lain`) VALUES
(5, 'ID00000003', 'Muhamad Syahrul', 'Gelombang 3', 'Ya', 'Tidak', 'Ya', 'Ya', 'Ya', 'Ya', 'Ya', 'Ya', 'Ya', 'Tidak'),
(6, 'ID00000001', 'Administrator', 'Gelombang 2', 'Ya', 'Tidak', 'Ya', 'Ya', 'Ya', 'Tidak', 'Tidak', 'Tidak', 'Ya', 'Tidak');

-- --------------------------------------------------------

--
-- Struktur dari tabel `persyaratan_tambahan`
--

CREATE TABLE `persyaratan_tambahan` (
  `id` int(11) NOT NULL,
  `siswa_id` varchar(10) NOT NULL,
  `nama_siswa` varchar(128) NOT NULL,
  `gelombang` varchar(25) NOT NULL,
  `surat_pindah_sekolah` varchar(10) NOT NULL,
  `surat_dapotik` varchar(10) NOT NULL,
  `raport_asli` varchar(10) NOT NULL,
  `validasi_nisn` varchar(10) NOT NULL,
  `sertifikat` varchar(10) NOT NULL,
  `lain_lain` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `persyaratan_tambahan`
--

INSERT INTO `persyaratan_tambahan` (`id`, `siswa_id`, `nama_siswa`, `gelombang`, `surat_pindah_sekolah`, `surat_dapotik`, `raport_asli`, `validasi_nisn`, `sertifikat`, `lain_lain`) VALUES
(3, 'ID00000003', 'Muhamad Syahrul', 'Gelombang 3', 'Tidak', 'Ya', 'Ya', 'Tidak', 'Tidak', 'Ya'),
(4, 'ID00000001', 'Administrator', 'Gelombang 2', 'Tidak', 'Tidak', 'Tidak', 'Tidak', 'Tidak', 'Tidak');

-- --------------------------------------------------------

--
-- Struktur dari tabel `registrasi_peserta_didik`
--

CREATE TABLE `registrasi_peserta_didik` (
  `id` int(11) NOT NULL,
  `keahlian` varchar(128) NOT NULL,
  `jenis_pendaftaran` varchar(50) NOT NULL,
  `nis` varchar(10) NOT NULL,
  `tanggal_masuk` varchar(10) NOT NULL,
  `asal_sekolah` varchar(128) NOT NULL,
  `nomor_ujian` varchar(10) NOT NULL,
  `no_ijazah` varchar(25) NOT NULL,
  `no_skhun` varchar(25) NOT NULL,
  `siswa_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `registrasi_peserta_didik`
--

INSERT INTO `registrasi_peserta_didik` (`id`, `keahlian`, `jenis_pendaftaran`, `nis`, `tanggal_masuk`, `asal_sekolah`, `nomor_ujian`, `no_ijazah`, `no_skhun`, `siswa_id`) VALUES
(12, 'Akuntansi', 'Siswa Baru', '3434', '2020-06-21', 'SMP 56 Jakarta', '1234', '23432948', '3243284', 'ID00000001'),
(13, 'Teknik Komputer Jaringan', 'Siswa Baru', '6738', '2020-06-22', 'SMP N 1 BOGOR', '9784', '3424398829', '3384932899', 'ID00000003');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_id`
--

CREATE TABLE `role_id` (
  `id` int(11) NOT NULL,
  `role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `role_id`
--

INSERT INTO `role_id` (`id`, `role`) VALUES
(1, 'Administrator'),
(2, 'Guru'),
(3, 'Siswa');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id_registrasi` varchar(128) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `role_id` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id_registrasi`, `nama`, `username`, `password`, `created_at`, `role_id`) VALUES
('ID00000001', 'Administrator', 'admin', 'admin', '2020-06-21 20:38:28', 1),
('ID00000002', 'Muhamad Nur Sukur', 'nur', '123456', '2020-06-21 18:23:03', 2),
('ID00000003', 'Muhamad Syahrul', 'syahrul', '1234', '2020-06-21 19:25:21', 3);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `data_ayah_kandung`
--
ALTER TABLE `data_ayah_kandung`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `data_beasiswa`
--
ALTER TABLE `data_beasiswa`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `data_guru`
--
ALTER TABLE `data_guru`
  ADD PRIMARY KEY (`id_guru`);

--
-- Indeks untuk tabel `data_ibu_kandung`
--
ALTER TABLE `data_ibu_kandung`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `data_kontak`
--
ALTER TABLE `data_kontak`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `data_prestasi`
--
ALTER TABLE `data_prestasi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `data_pribadi_siswa`
--
ALTER TABLE `data_pribadi_siswa`
  ADD PRIMARY KEY (`no_registrasi`);

--
-- Indeks untuk tabel `data_priodik`
--
ALTER TABLE `data_priodik`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `data_wali`
--
ALTER TABLE `data_wali`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `form_berkas`
--
ALTER TABLE `form_berkas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `jenis_pembayaran`
--
ALTER TABLE `jenis_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pendaftaran_keluar`
--
ALTER TABLE `pendaftaran_keluar`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `penyerahan_berkas`
--
ALTER TABLE `penyerahan_berkas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `persyaratan_tambahan`
--
ALTER TABLE `persyaratan_tambahan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `registrasi_peserta_didik`
--
ALTER TABLE `registrasi_peserta_didik`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `role_id`
--
ALTER TABLE `role_id`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_registrasi`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `data_ayah_kandung`
--
ALTER TABLE `data_ayah_kandung`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `data_beasiswa`
--
ALTER TABLE `data_beasiswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `data_ibu_kandung`
--
ALTER TABLE `data_ibu_kandung`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `data_kontak`
--
ALTER TABLE `data_kontak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `data_prestasi`
--
ALTER TABLE `data_prestasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT untuk tabel `data_priodik`
--
ALTER TABLE `data_priodik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `data_wali`
--
ALTER TABLE `data_wali`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `form_berkas`
--
ALTER TABLE `form_berkas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `jenis_pembayaran`
--
ALTER TABLE `jenis_pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT untuk tabel `pendaftaran_keluar`
--
ALTER TABLE `pendaftaran_keluar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `penyerahan_berkas`
--
ALTER TABLE `penyerahan_berkas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `persyaratan_tambahan`
--
ALTER TABLE `persyaratan_tambahan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `registrasi_peserta_didik`
--
ALTER TABLE `registrasi_peserta_didik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `role_id`
--
ALTER TABLE `role_id`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role_id` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
