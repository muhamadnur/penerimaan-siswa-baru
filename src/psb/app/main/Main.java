/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psb.app.main;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import koneksi.MyConnection;
import menu.MenuItem;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.swing.JRViewer;
import net.sf.jasperreports.view.JasperViewer;
import psb.app.auth.Login;
import psb.app.main.form.FormGantiPassword;
import psb.app.main.form.FormWali;
import psb.app.main.panel.database.data_guru.GuruPanel;
import psb.app.main.panel.database.data_pembayaran.KartuPelajarPanel;
import psb.app.main.panel.database.data_pembayaran.KoperasiPanel;
import psb.app.main.panel.database.data_pembayaran.MPLSPanel;
import psb.app.main.panel.database.data_pembayaran.OsisPanel;
import psb.app.main.panel.pendaftaran.OrangTuaPanel;
import psb.app.main.panel.pendaftaran.RincianPesertaDidik;
import psb.app.main.panel.database.data_pembayaran.PembayaranDaftarUlang;
import psb.app.main.panel.database.data_pembayaran.PembayaranPanel;
import psb.app.main.panel.database.data_pembayaran.PembayaranSaranaGedung;
import psb.app.main.panel.database.data_pembayaran.PembayaranSpp;
import psb.app.main.panel.database.data_pembayaran.PembayaranUangPengembangan;
import psb.app.main.panel.database.data_pembayaran.PsikotesPanel;
import psb.app.main.panel.database.data_pembayaran.SaranaPraktekPanel;
import psb.app.main.panel.database.data_pembayaran.SeragamPanel;
import psb.app.main.panel.database.data_siswa.DataAyahKandung;
import psb.app.main.panel.database.data_siswa.DataKontak;
import psb.app.main.panel.database.data_siswa.DataPriodik;
import psb.app.main.panel.database.data_siswa.DataWaliSiswa;
import psb.app.main.panel.database.data_siswa.DataIbuKandung;
import psb.app.main.panel.formBerkas.DataPenyerahanBerkasPanel;
import psb.app.main.panel.database.data_siswa.DataPribadiSiswa;
import psb.app.main.panel.pendaftaran.DataPribadiPanel;
import session.UserSession;

/**
 *
 * @author Admin
 */
public class Main extends javax.swing.JFrame {

    Statement st;
    ResultSet rs;
    MyConnection koneksi;
    JpanelLoader jpload = new JpanelLoader();
    String id_registrasi = UserSession.getId_registrasi();
    String nama = UserSession.getNama();
    int role_id = UserSession.getRole_id();
    String userMenu = String.valueOf(role_id);

    /**
     * Creates new form Main
     */
    public Main() {
        initComponents();
        koneksi = new MyConnection();
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setLocationRelativeTo(null);
        txt_nama.setText(nama);
        HomePanel home = new HomePanel();
        jpload.jPanelLoader(panelBody, home);
        execute();

    }

    private void execute() {
        ImageIcon iconHome = new ImageIcon(getClass().getResource("/menu/icon_home.png"));
        ImageIcon icontransaction = new ImageIcon(getClass().getResource("/menu/transaction.png"));
        ImageIcon iconreport = new ImageIcon(getClass().getResource("/menu/report.png"));
        ImageIcon iconDatabase = new ImageIcon(getClass().getResource("/menu/database.png"));
        ImageIcon iconSubMenu = new ImageIcon(getClass().getResource("/menu/subMenu.png"));
        ImageIcon iconNext = new ImageIcon(getClass().getResource("/menu/next.png"));
        ImageIcon iconLogout = new ImageIcon(getClass().getResource("/menu/logout.png"));
        ImageIcon iconEmployee = new ImageIcon(getClass().getResource("/menu/user.png"));
        ImageIcon iconForm = new ImageIcon(getClass().getResource("/menu/icon_form.png"));
        ImageIcon iconKey = new ImageIcon(getClass().getResource("/menu/icon_key.png"));
        //home
        MenuItem menuHome = new MenuItem(iconHome, "Home", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HomePanel home = new HomePanel();
                jpload.jPanelLoader(panelBody, home);
            }
        });
        //pendaftaran siswa
        MenuItem data_pribadi = new MenuItem(iconSubMenu, "Data Pribadi", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                DataPribadiPanel pro = new DataPribadiPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem ortu = new MenuItem(iconNext, "Data Orang Tua", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                OrangTuaPanel pro = new OrangTuaPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem wali = new MenuItem(iconNext, "Data Wali", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                FormWali pro = new FormWali();
                pro.pack();
                pro.setVisible(true);
                pro.setLocationRelativeTo(null);
                pro.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            }
        });
        MenuItem datawalimurid = new MenuItem(iconSubMenu, "Data Wali Murid", null, ortu, wali);
        MenuItem rincian = new MenuItem(iconSubMenu, "Data Rincian Peserta Didik", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {

                RincianPesertaDidik pro = new RincianPesertaDidik();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem menuPendaftaran = new MenuItem(iconEmployee, "Pendaftaran", null, data_pribadi, datawalimurid, rincian);

        //pembayaran
        MenuItem menuPembayaran = new MenuItem(icontransaction, "Pembayaran", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PembayaranPanel pro = new PembayaranPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });

        //Menu Database
        //data siswa
        MenuItem dataPribadi = new MenuItem(iconNext, "Data Pribadi", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DataPribadiSiswa pro = new DataPribadiSiswa();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem dataAyahkandung = new MenuItem(iconNext, "Data Ayah Kandung", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DataAyahKandung pro = new DataAyahKandung();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem dataIbukandung = new MenuItem(iconNext, "Data Ibu Kandung", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DataIbuKandung pro = new DataIbuKandung();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem dataWali = new MenuItem(iconNext, "Data Wali", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DataWaliSiswa pro = new DataWaliSiswa();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem kontak = new MenuItem(iconNext, "Kontak", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DataKontak pro = new DataKontak();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem dataPriodik = new MenuItem(iconNext, "Data Priodik", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DataPriodik pro = new DataPriodik();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem berkas = new MenuItem(iconForm, "Form Penyerahan Berkas", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DataPenyerahanBerkasPanel pro = new DataPenyerahanBerkasPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem item1 = new MenuItem(iconSubMenu, "Data Siswa", null, dataPribadi, dataAyahkandung, dataIbukandung, dataWali, dataPriodik, kontak);

        //data guru
        MenuItem item2 = new MenuItem(iconSubMenu, "Data Guru", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GuruPanel pro = new GuruPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });

        //data pembayaran
        MenuItem spp = new MenuItem(iconNext, "Pembayaran SPP", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                PembayaranSpp pro = new PembayaranSpp();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem daftarUlang = new MenuItem(iconNext, "Pembayaran Daftar Ulang", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                PembayaranDaftarUlang pro = new PembayaranDaftarUlang();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem pengembangan = new MenuItem(iconNext, "Pembayaran Uang Pengembangan", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                PembayaranUangPengembangan pro = new PembayaranUangPengembangan();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem gedung = new MenuItem(iconNext, "Pembayaran Sarana Gedung", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                PembayaranSaranaGedung pro = new PembayaranSaranaGedung();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem pemMPLS = new MenuItem(iconNext, "Pembayaran MPLS", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                MPLSPanel pro = new MPLSPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem pemPsikotes = new MenuItem(iconNext, "Pembayaran Psikotes", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                PsikotesPanel pro = new PsikotesPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem pemSaranaPraktek = new MenuItem(iconNext, "Pembayaran Sarana Praktek", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                SaranaPraktekPanel pro = new SaranaPraktekPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem pemKoperasi = new MenuItem(iconNext, "Pembayaran Koperasi", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                KoperasiPanel pro = new KoperasiPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem pemOsis = new MenuItem(iconNext, "Pembayaran OSIS", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                OsisPanel pro = new OsisPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem pemKartuPelajar = new MenuItem(iconNext, "Pembayaran Kartu Pelajar", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                KartuPelajarPanel pro = new KartuPelajarPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem pemSeragam = new MenuItem(iconNext, "Pembayaran Seragam", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                SeragamPanel pro = new SeragamPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });

        MenuItem item3 = new MenuItem(iconSubMenu, "Data Pembayaran", null,
                spp, daftarUlang, pengembangan, gedung, pemMPLS, pemPsikotes, pemSaranaPraktek, pemKoperasi,
                pemOsis, pemKartuPelajar, pemSeragam);
        
       
        //create menu system database
        MenuItem menuDatabase = new MenuItem(iconDatabase, "Database", null, item1, item2, item3);
        //create menu system database
        MenuItem role2 = new MenuItem(iconDatabase, "Database", null, item1, item3);
        //report database
        MenuItem data_siswa = new MenuItem(iconNext, "Report Data Siswa", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printDataSiswa();
            }
        });
        MenuItem form_berkasItem = new MenuItem(iconNext, "Report Penyerahan Berkas", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printBerkas();
            }
        });
        MenuItem data_guru = new MenuItem(iconNext, "Report Data Guru", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printDataGuru();
            }
        });

        MenuItem reportDatabase = new MenuItem(iconSubMenu, "Report Database", null, data_siswa, form_berkasItem, data_guru);

        //jurusan akuntansi
        MenuItem akuntansi = new MenuItem(iconNext, "Akuntansi", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printAkuntansi();
            }
        });
        //jurusan pemasaran
        MenuItem Pemasaran = new MenuItem(iconNext, "Pemasaran", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printPemasaran();
            }
        });
        //jurusan Teknik Komputer Jaringan
        MenuItem Komputer = new MenuItem(iconNext, "Teknik Komputer Jaringan", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printKomputer();
            }
        });
        //jurusan Teknik Sepeda Motor
        MenuItem Motor = new MenuItem(iconNext, "Teknik Sepeda Motor", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printMotor();
            }
        });
        //report jurusan
        MenuItem reportJurusan = new MenuItem(iconSubMenu, "Report Jurusan", null, akuntansi, Pemasaran, Komputer, Motor);
        //report pembayaran
        MenuItem today = new MenuItem(iconNext, "Pembayaran Hari Ini", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printDataToday();
            }
        });
        MenuItem daftar_ulang = new MenuItem(iconNext, "Daftar Ulang", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printDataPembayaranDaftar();
            }
        });
        MenuItem uang_p = new MenuItem(iconNext, "Uang Pengembangan", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printDataUangPengembangan();
            }
        });
        MenuItem pem_spp = new MenuItem(iconNext, "SPP", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printDataPembayaranSpp();
            }
        });
        MenuItem printPendaftaran = new MenuItem(iconNext, "Pendaftaran", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printPendaftaran();
            }
        });
        MenuItem printSarana = new MenuItem(iconNext, "Sarana Gedung", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printSarana();
            }
        });
        MenuItem MPLS = new MenuItem(iconNext, "MPLS", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MPLS();
            }
        });
        MenuItem Psikotes = new MenuItem(iconNext, "Psikotes", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Psikotes();
            }
        });
        MenuItem SaranaPraktek = new MenuItem(iconNext, "Sarana Praktek", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SaranaPraktek();
            }
        });
        MenuItem koperasi = new MenuItem(iconNext, "Koperasi", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Koperasi();
            }
        });
        MenuItem osis = new MenuItem(iconNext, "OSIS", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OSIS();
            }
        });
        MenuItem kartuPelajar = new MenuItem(iconNext, "Kartu Pelajar", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                KartuPelajar();
            }
        });
        MenuItem seragam = new MenuItem(iconNext, "Seragam", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Seragam();
            }
        });
        MenuItem reportPembayaran = new MenuItem(iconSubMenu, "Report Pembayaran", null, today,daftar_ulang,
                uang_p, pem_spp, printPendaftaran, printSarana, MPLS, Psikotes, SaranaPraktek, koperasi, osis, kartuPelajar, seragam);
        MenuItem menuReport = new MenuItem(iconreport, "Report", null, reportDatabase, reportPembayaran, reportJurusan);
        //ganti password
        MenuItem key = new MenuItem(iconKey, "Ganti Password", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FormGantiPassword pass = new FormGantiPassword();
                pass.setVisible(true);
                pass.pack();
                pass.setLocationRelativeTo(null);
                pass.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            }
        });
        //menu logout
        MenuItem menuLogout = new MenuItem(iconLogout, "Logout", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int ok = JOptionPane.showConfirmDialog(null, "Logout and exit", "Message", 2, JOptionPane.YES_NO_OPTION);
                if (ok == 0) {
                    JOptionPane.showMessageDialog(null, "Succes Logout");
                    Login login = new Login();
                    login.setVisible(true);
                    login.pack();
                    login.setLocationRelativeTo(null);
                    login.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    dispose();
                }
            }
        });

//        addMenu( menuHome, menuPendaftaran, menuPembayaran, menuDatabase, menuReport, menuLogout);
        switch (userMenu) {
            case "1":
                addMenu(menuHome, menuPendaftaran, berkas, menuPembayaran, menuDatabase, menuReport, key, menuLogout);
                break;
            case "2":
                addMenu(menuHome, berkas, menuPembayaran, role2, menuReport, key, menuLogout);
                break;
            case "3":
                addMenu(menuHome, menuPendaftaran, key, menuLogout);
                break;
        }

    }
    private void printDataToday(){
        Date dt = new Date();
        SimpleDateFormat fm = new SimpleDateFormat("dd-MM-yyyy");
        String today = fm.format(dt);
        try {
            String namaFile = "src\\report\\reportPembayaran_1.jasper";
            HashMap param = new HashMap();
            param.put("today", today);  // no_faktur  is ireport parameter name
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        
    }
    private void printMotor() {
        String daftar = "Teknik Sepeda Motor";
        try {
            String namaFile = "src\\report\\reportJurusan.jasper";
            HashMap param = new HashMap();
            param.put("jurusan", daftar);  // no_faktur  is ireport parameter name
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    private void printKomputer() {
        String daftar = "Teknik Komputer Jaringan";
        try {
            String namaFile = "src\\report\\reportJurusan.jasper";
            HashMap param = new HashMap();
            param.put("jurusan", daftar);  // no_faktur  is ireport parameter name
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    private void printPemasaran() {
        String daftar = "Pemasaran";
        try {
            String namaFile = "src\\report\\reportJurusan.jasper";
            HashMap param = new HashMap();
            param.put("jurusan", daftar);  // no_faktur  is ireport parameter name
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    private void printAkuntansi() {
        String daftar = "Akuntansi";
        try {
            String namaFile = "src\\report\\reportJurusan.jasper";
            HashMap param = new HashMap();
            param.put("jurusan", daftar);  // no_faktur  is ireport parameter name
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    private void Seragam() {
        String daftar = "Seragam";
        try {
            String namaFile = "src\\report\\reportPembayaran.jasper";
            HashMap param = new HashMap();
            param.put("jenis_pem", daftar);  // no_faktur  is ireport parameter name
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void KartuPelajar() {
        String daftar = "Kartu Pelajar";
        try {
            String namaFile = "src\\report\\reportPembayaran.jasper";
            HashMap param = new HashMap();
            param.put("jenis_pem", daftar);  // no_faktur  is ireport parameter name
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void OSIS() {
        String daftar = "OSIS";
        try {
            String namaFile = "src\\report\\reportPembayaran.jasper";
            HashMap param = new HashMap();
            param.put("jenis_pem", daftar);  // no_faktur  is ireport parameter name
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void Koperasi() {
        String daftar = "Koperasi";
        try {
            String namaFile = "src\\report\\reportPembayaran.jasper";
            HashMap param = new HashMap();
            param.put("jenis_pem", daftar);  // no_faktur  is ireport parameter name
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void SaranaPraktek() {
        String daftar = "Sarana Praktek";
        try {
            String namaFile = "src\\report\\reportPembayaran.jasper";
            HashMap param = new HashMap();
            param.put("jenis_pem", daftar);  // no_faktur  is ireport parameter name
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void Psikotes() {
        String daftar = "Psikotes";
        try {
            String namaFile = "src\\report\\reportPembayaran.jasper";
            HashMap param = new HashMap();
            param.put("jenis_pem", daftar);  // no_faktur  is ireport parameter name
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void MPLS() {
        String daftar = "MPLS";
        try {
            String namaFile = "src\\report\\reportPembayaran.jasper";
            HashMap param = new HashMap();
            param.put("jenis_pem", daftar);  // no_faktur  is ireport parameter name
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void printSarana() {
        String daftar = "Sarana Gedung";
        try {
            String namaFile = "src\\report\\reportPembayaran.jasper";
            HashMap param = new HashMap();
            param.put("jenis_pem", daftar);  // no_faktur  is ireport parameter name
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void printPendaftaran() {
        String daftar = "Pendaftaran";
        try {
            String namaFile = "src\\report\\reportPembayaran.jasper";
            HashMap param = new HashMap();
            param.put("jenis_pem", daftar);  // no_faktur  is ireport parameter name
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void printBerkas() {
        try {
            String namaFile = "src\\report\\reportPenyerahanBerkas.jasper";
            HashMap param = new HashMap();
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getContentPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void printDataUangPengembangan() {
        String up = "Uang Pengembangan";
        try {
            String namaFile = "src\\report\\reportPembayaran.jasper";
            HashMap param = new HashMap();
            param.put("jenis_pem", up);  // no_faktur  is ireport parameter name
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void printDataPembayaranDaftar() {
        String daftar = "Pembayaran Daftar Ulang";
        try {
            String namaFile = "src\\report\\reportPembayaran.jasper";
            HashMap param = new HashMap();
            param.put("jenis_pem", daftar);  // no_faktur  is ireport parameter name
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void printDataPembayaranSpp() {
        String spp = "Pembayaran SPP";
        try {
            String namaFile = "src\\report\\reportPembayaran.jasper";
            HashMap param = new HashMap();
            param.put("jenis_pem", spp);  // no_faktur  is ireport parameter name
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void printDataGuru() {
        try {
            String namaFile = "src\\report\\reportGuru.jasper";
            HashMap param = new HashMap();
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getContentPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void printDataSiswa() {
        try {
            String namaFile = "src\\report\\reportDataSiswa.jasper";
            HashMap param = new HashMap();
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getContentPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void addMenu(MenuItem... menu) {
        for (int i = 0; i < menu.length; i++) {
            menus.add(menu[i]);
            ArrayList<MenuItem> subMenu = menu[i].getSubMenu();
            for (MenuItem m : subMenu) {
                addMenu(m);
            }
        }
        menus.revalidate();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelHeader = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_nama = new javax.swing.JLabel();
        panelMenu = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        menus = new javax.swing.JPanel();
        panelBody = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        panelHeader.setBackground(new java.awt.Color(0, 102, 204));
        panelHeader.setPreferredSize(new java.awt.Dimension(561, 50));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("SMK ISLAM AR RIDHO");

        txt_nama.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txt_nama.setForeground(new java.awt.Color(255, 255, 255));
        txt_nama.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txt_nama.setText("Nama");

        javax.swing.GroupLayout panelHeaderLayout = new javax.swing.GroupLayout(panelHeader);
        panelHeader.setLayout(panelHeaderLayout);
        panelHeaderLayout.setHorizontalGroup(
            panelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelHeaderLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jLabel1)
                .addContainerGap(678, Short.MAX_VALUE))
            .addGroup(panelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelHeaderLayout.createSequentialGroup()
                    .addContainerGap(714, Short.MAX_VALUE)
                    .addComponent(txt_nama, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
        );
        panelHeaderLayout.setVerticalGroup(
            panelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelHeaderLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(panelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelHeaderLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(txt_nama, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        getContentPane().add(panelHeader, java.awt.BorderLayout.PAGE_START);

        panelMenu.setBackground(new java.awt.Color(0, 102, 204));
        panelMenu.setPreferredSize(new java.awt.Dimension(250, 384));

        jScrollPane1.setBorder(null);

        menus.setBackground(new java.awt.Color(255, 255, 255));
        menus.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        menus.setLayout(new javax.swing.BoxLayout(menus, javax.swing.BoxLayout.Y_AXIS));
        jScrollPane1.setViewportView(menus);

        javax.swing.GroupLayout panelMenuLayout = new javax.swing.GroupLayout(panelMenu);
        panelMenu.setLayout(panelMenuLayout);
        panelMenuLayout.setHorizontalGroup(
            panelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMenuLayout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE)
                .addGap(8, 8, 8))
        );
        panelMenuLayout.setVerticalGroup(
            panelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 407, Short.MAX_VALUE)
        );

        getContentPane().add(panelMenu, java.awt.BorderLayout.LINE_START);

        panelBody.setBackground(new java.awt.Color(255, 255, 255));
        panelBody.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout panelBodyLayout = new javax.swing.GroupLayout(panelBody);
        panelBody.setLayout(panelBodyLayout);
        panelBodyLayout.setHorizontalGroup(
            panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 636, Short.MAX_VALUE)
        );
        panelBodyLayout.setVerticalGroup(
            panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 403, Short.MAX_VALUE)
        );

        getContentPane().add(panelBody, java.awt.BorderLayout.CENTER);

        setSize(new java.awt.Dimension(906, 496));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel menus;
    private javax.swing.JPanel panelBody;
    private javax.swing.JPanel panelHeader;
    private javax.swing.JPanel panelMenu;
    public javax.swing.JLabel txt_nama;
    // End of variables declaration//GEN-END:variables

}
