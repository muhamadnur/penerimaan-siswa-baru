/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psb.app.main.form;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import koneksi.MyConnection;
import psb.app.auth.Login;
import psb.app.main.panel.database.data_guru.GuruPanel;

/**
 *
 * @author Admin
 */
public class FormDataGuru extends javax.swing.JFrame {

    String id = "";

    /**
     * Creates new form FormDataGuru
     */
    public FormDataGuru() {
        initComponents();
        this.setLocationRelativeTo(null);
        txt_id_guru.setEditable(false);
        txt_nama_guru.requestFocus();
        autonumber();
    }

    private void autonumber() {
        PreparedStatement ps;
        ResultSet rs;
        try {
            String query = "select MAX(RIGHT(id_registrasi,8)) AS NO  from users order by id_registrasi desc";;
            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            while (rs.next()) {
                if (rs.first() == false) {
                    txt_id_guru.setText("ID10000001");
                } else {
                    rs.last();
                    int auto_id = rs.getInt(1) + 1;
                    String no = String.valueOf(auto_id);
                    int Nomor = no.length();
                    //MENGATUR jumlah 0
                    for (int j = 0; j < 8 - Nomor; j++) {
                        no = "0" + no;
                    }
                    txt_id_guru.setText("ID" + no);
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();//penanganan masalah
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel7 = new javax.swing.JPanel();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        txt_nama_guru = new javax.swing.JTextField();
        txt_email_guru = new javax.swing.JTextField();
        txt_telephone = new javax.swing.JTextField();
        txt_id_guru = new javax.swing.JTextField();
        jLabel38 = new javax.swing.JLabel();
        btn_batal = new javax.swing.JButton();
        btn_ubah = new javax.swing.JButton();
        btn_simpan = new javax.swing.JButton();
        jLabel39 = new javax.swing.JLabel();
        txt_alamat = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        txt_username = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel34.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel34.setText("DATA GURU");

        jLabel35.setText("Nama Guru");

        jLabel36.setText("Email");

        jLabel37.setText("Telephone");

        jLabel38.setText("ID Guru");

        btn_batal.setBackground(new java.awt.Color(255, 102, 102));
        btn_batal.setForeground(new java.awt.Color(255, 255, 255));
        btn_batal.setText("Batal");
        btn_batal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_batalActionPerformed(evt);
            }
        });

        btn_ubah.setBackground(new java.awt.Color(0, 102, 204));
        btn_ubah.setForeground(new java.awt.Color(255, 255, 255));
        btn_ubah.setText("Ubah");
        btn_ubah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ubahActionPerformed(evt);
            }
        });

        btn_simpan.setBackground(new java.awt.Color(153, 255, 255));
        btn_simpan.setText("Simpan");
        btn_simpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_simpanActionPerformed(evt);
            }
        });

        jLabel39.setText("Alamat");

        jLabel1.setText("Username");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel34)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel39, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(jLabel36, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel38, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel35, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 92, Short.MAX_VALUE)
                                        .addComponent(jLabel37, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(5, 5, 5)
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txt_email_guru, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txt_telephone)
                                    .addComponent(txt_id_guru)
                                    .addComponent(txt_nama_guru)
                                    .addComponent(txt_alamat)
                                    .addComponent(txt_username)))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                        .addContainerGap(178, Short.MAX_VALUE)
                        .addComponent(btn_batal)
                        .addGap(8, 8, 8)
                        .addComponent(btn_ubah)
                        .addGap(8, 8, 8)
                        .addComponent(btn_simpan)))
                .addGap(10, 10, 10))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jLabel34)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_id_guru, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel38))
                .addGap(6, 6, 6)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel35)
                    .addComponent(txt_nama_guru, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel36)
                    .addComponent(txt_email_guru, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txt_username, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel37)
                    .addComponent(txt_telephone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel39)
                    .addComponent(txt_alamat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_simpan)
                    .addComponent(btn_ubah)
                    .addComponent(btn_batal))
                .addGap(30, 30, 30))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 389, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 268, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_ubahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ubahActionPerformed
        // TODO add your handling code here:
        String id = txt_id_guru.getText();
        String nama = txt_nama_guru.getText();
        String email = txt_email_guru.getText();
        String telephone = txt_telephone.getText();
        String alamat = txt_alamat.getText();
        if (id.isEmpty()) {
            JOptionPane.showMessageDialog(null, "ID Siswa tidak boleh kosong");
        } else if (nama.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Nama tidak boleh kosong");
            txt_nama_guru.requestFocus();
        } else if (email.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Email tidak boleh kosong");
            txt_email_guru.requestFocus();
        } else if (telephone.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Telephone tidak boleh kosong");
            txt_telephone.requestFocus();
        } else if (alamat.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Alamat tidak boleh kosong");
            txt_alamat.requestFocus();
        } else {
            updateDataGuru();
            updateUser();
            JOptionPane.showMessageDialog(null, "Data berhasil di ubah");
            kosong();
            this.dispose();

        }
    }//GEN-LAST:event_btn_ubahActionPerformed
    private void updateDataGuru() {
        String id = txt_id_guru.getText();
        String nama = txt_nama_guru.getText();
        String email = txt_email_guru.getText();
        String telephone = txt_telephone.getText();
        String alamat = txt_alamat.getText();
        String username = txt_username.getText();
        String query = "UPDATE `data_guru` SET `id_guru`=?,`nama_guru`=?,`email_guru`=?,`username`=?,`nomor_telephone`=?,"
                + "`alamat`=? WHERE `id_guru`='" + id + "'";
        try {
            PreparedStatement stat = MyConnection.getConnection().prepareStatement(query);
            stat.setString(1, id);
            stat.setString(2, nama);
            stat.setString(3, email);
            stat.setString(4, username);
            stat.setString(5, telephone);
            stat.setString(6, alamat);

            stat.executeUpdate();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Data gagal di ubah" + e);
        }
    }

    private void updateUser() {
        String id = txt_id_guru.getText();
        String nama = txt_nama_guru.getText();
        String username = txt_username.getText();
        String query = "UPDATE `users` SET `id_registrasi`=?,`nama`=?,`username`=? WHERE `id_registrasi`='" + id + "'";
        try {
            PreparedStatement stat = MyConnection.getConnection().prepareStatement(query);
            stat.setString(1, id);
            stat.setString(2, nama);
            stat.setString(3, username);

            stat.executeUpdate();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Data gagal di ubah" + e);
        }
    }
    private void btn_simpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_simpanActionPerformed
        // TODO add your handling code here:
        saveDataGuru();
        saveUser();
        JOptionPane.showMessageDialog(null, "Data berhasil di simpan");
        kosong();
        autonumber();
    }//GEN-LAST:event_btn_simpanActionPerformed

    private void btn_batalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_batalActionPerformed
        // TODO add your handling code here:
        kosong();
        this.dispose();
    }//GEN-LAST:event_btn_batalActionPerformed
    private void saveUser() {
        String id = txt_id_guru.getText();
        String username = txt_username.getText();
        String nama = txt_nama_guru.getText();
        if (id.isEmpty()) {
            JOptionPane.showMessageDialog(null, "ID Siswa tidak boleh kosong");
        } else if (username.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Username tidak boleh kosong");
            txt_username.requestFocus();
        } else {
            String password = "123456";
            String query = "INSERT INTO `users`(`id_registrasi`, `nama`, `username`, `password`, `role_id`) VALUES (?,?,?,?,2)";
            try {
                PreparedStatement stat = MyConnection.getConnection().prepareStatement(query);
                stat.setString(1, id);
                stat.setString(2, nama);
                stat.setString(3, username);
                stat.setString(4, password);

                stat.executeUpdate();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Data gagal di simpan" + e);
            }
        }
    }

    private void saveDataGuru() {
        String id = txt_id_guru.getText();
        String nama = txt_nama_guru.getText();
        String email = txt_email_guru.getText();
        String telephone = txt_telephone.getText();
        String alamat = txt_alamat.getText();
        String username = txt_username.getText();
        if (id.isEmpty()) {
            JOptionPane.showMessageDialog(null, "ID Siswa tidak boleh kosong");
        } else if (nama.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Nomor handphone tidak boleh kosong");
            txt_nama_guru.requestFocus();
        } else if (email.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Email tidak boleh kosong");
            txt_email_guru.requestFocus();
        } else if (telephone.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Email tidak boleh kosong");
            txt_telephone.requestFocus();
        } else if (alamat.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Email tidak boleh kosong");
            txt_alamat.requestFocus();
        } else {
            String query = "INSERT INTO `data_guru`(`id_guru`, `nama_guru`, `email_guru`,`username`, `nomor_telephone`, `alamat`) VALUES (?,?,?,?,?,?)";
            try {
                PreparedStatement stat = MyConnection.getConnection().prepareStatement(query);
                stat.setString(1, id);
                stat.setString(2, nama);
                stat.setString(3, email);
                stat.setString(4, username);
                stat.setString(5, telephone);
                stat.setString(6, alamat);
                stat.executeUpdate();
//            JOptionPane.showMessageDialog(null, "Data berhasil di simpan");    
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Data gagal di simpan" + e);
            }
        }
    }

    private void kosong() {
        txt_nama_guru.setText("");
        txt_email_guru.setText("");
        txt_telephone.setText("");
        txt_alamat.setText("");
        txt_username.setText("");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormDataGuru.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormDataGuru.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormDataGuru.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormDataGuru.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FormDataGuru().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_batal;
    public javax.swing.JButton btn_simpan;
    public javax.swing.JButton btn_ubah;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JPanel jPanel7;
    public javax.swing.JTextField txt_alamat;
    public javax.swing.JTextField txt_email_guru;
    public javax.swing.JTextField txt_id_guru;
    public javax.swing.JTextField txt_nama_guru;
    public javax.swing.JTextField txt_telephone;
    public javax.swing.JTextField txt_username;
    // End of variables declaration//GEN-END:variables
}
