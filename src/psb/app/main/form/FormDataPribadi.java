/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psb.app.main.form;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import koneksi.MyConnection;
import psb.app.auth.Register;
import psb.app.main.Main;

/**
 *
 * @author Admin
 */
public class FormDataPribadi extends javax.swing.JFrame {

    Main main = new Main();
    String gender = null;
    String kewarganegaraan = null;
    String penerima_kps = null;
    String usulan_dari_sekolah = null;
    String kartu_kip = null;
    String penerima_kip = null;
    String id = null;

    /**
     * Creates new form FormDataPribadi
     */
    public FormDataPribadi() {
        initComponents();
        autonumber();
    }

    private void kosong() {
//        jComboBerkubutuhan.setSelectedItem(null);
//        jComboAgama.setSelectedItem(null);
//        jComboTempatTinggal.setSelectedItem(null);
//        jComboModaTransportasi.setSelectedItem(null);
//        jComboPip.setSelectedItem(null);
        jDateChooser.setDate(null);
        txt_nama_siswa.setText("");
        txt_nisn.setText("");
        txt_nik.setText("");
        txt_tempat_lahir.setText("");
        txt_no_regist_akta.setText("");
        txt_nama_negara.setText("");
        txt_alamat_jalan.setText("");
        txt_rt.setText("");
        txt_rw.setText("");
        txt_nama_dusun.setText("");
        txt_desa.setText("");
        txt_kecamatan.setText("");
        txt_kode_pos.setText("");
        txt_lintang.setText("");
        txt_bujur.setText("");
        txt_nomor_kks.setText("");
        txt_anak_ke.setText("");
        txt_no_kps.setText("");
        txt_nomor_kip.setText("");
        txt_nama_kip.setText("");
        txt_bank.setText("");
        txt_nomor_rekening.setText("");
        txt_nama_rekening.setText("");
    }

    public boolean checkUser(String id) {
        PreparedStatement ps;
        ResultSet rs;
        boolean id_exist = false;
        String query = "SELECT * FROM `data_pribadi_siswa` WHERE `no_registrasi` =?";
        try {
            ps = MyConnection.getConnection().prepareStatement(query);
            ps.setString(1, id);

            rs = ps.executeQuery();

            if (rs.next()) {
                id_exist = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
        }
        return id_exist;
    }

    private void autonumber() {
        PreparedStatement ps;
        ResultSet rs;
        try {
            String query = "select MAX(RIGHT(id_registrasi,8)) AS NO  from users order by id_registrasi desc";;
            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            while (rs.next()) {
                if (rs.first() == false) {
                    txt_registrasi.setText("ID10000001");
                } else {
                    rs.last();
                    int auto_id = rs.getInt(1) + 1;
                    String no = String.valueOf(auto_id);
                    int Nomor = no.length();
                    //MENGATUR jumlah 0
                    for (int j = 0; j < 8 - Nomor; j++) {
                        no = "0" + no;
                    }
                    txt_registrasi.setText("ID" + no);
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();//penanganan masalah
        }

    }

    private void simpanData() {
        if (r_laki.isSelected()) {
            gender = "Laki-laki";
        } else if (r_perempuan.isSelected()) {
            gender = "Perempuan";
        } else {
            JOptionPane.showMessageDialog(null, "Jenis Kelamin tidak boleh kosong");
        }
        if (r_wni.isSelected()) {
            kewarganegaraan = "WNI";
        } else if (r_wna.isSelected()) {
            kewarganegaraan = "WNA";
        } else {
            JOptionPane.showMessageDialog(null, "Kewarganegaraan tidak boleh kosong");
        }
        if (r_ya_kps.isSelected()) {
            penerima_kps = "Ya";
        } else if (r_tidak_kps.isSelected()) {
            penerima_kps = "Tidak";
        } else {
            JOptionPane.showMessageDialog(null, "Penerima KPS tidak boleh kosong");
        }
        if (r_ya_pip.isSelected()) {
            usulan_dari_sekolah = "Ya";
        } else if (r_tidak_pip.isSelected()) {
            usulan_dari_sekolah = "Tidak";
        } else {
            JOptionPane.showMessageDialog(null, "Usulan dari sekolah tidak boleh kosong");
        }
        if (r_ya_kip.isSelected()) {
            penerima_kip = "Ya";
        } else if (r_tidak_kip.isSelected()) {
            penerima_kip = "Tidak";
        } else {
            JOptionPane.showMessageDialog(null, "Penerima KIP tidak boleh kosong");
        }
        if (r_ya_kartu.isSelected()) {
            kartu_kip = "Ya";
        } else if (r_tidak_kartu.isSelected()) {
            kartu_kip = "Tidak";
        } else {
            JOptionPane.showMessageDialog(null, "Kartu KIP tidak boleh kosong");
        }
        String query = "INSERT INTO data_pribadi_siswa(no_registrasi,"
                + " nama_lengkap, jenis_kelamin, nisn, nik, tempat_lahir, tanggal_lahir, "
                + "nomor_akta_lahir, agama,kewarganegaraan, nama_negara, berkebutuhan_khusus,"
                + " alamat_jalan, rt, rw, nama_dusun, nama_desa, kecamatan, kode_pos, lintang, "
                + "bujur, tempat_tinggal, moda_transportasi, nomor_kks, anak_keberapa, penerima_kps, "
                + "nomor_kps, usulan_dari_sekolah, penerima_kip, nomor_kip, nama_kip, kartu_kip,"
                + " alasan_layak_pip, bank, nomor_rekening, nama_rekening) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String tanggal = String.valueOf(dateFormat.format(jDateChooser.getDate()));
        try {
            PreparedStatement stat = MyConnection.getConnection().prepareStatement(query);
            stat.setString(1, txt_registrasi.getText());
            stat.setString(2, txt_nama_siswa.getText());
            stat.setString(3, gender);
            stat.setString(4, txt_nisn.getText());
            stat.setString(5, txt_nik.getText());
            stat.setString(6, txt_tempat_lahir.getText());
            stat.setString(7, tanggal);
            stat.setString(8, txt_no_regist_akta.getText());
            stat.setString(9, jComboAgama.getSelectedItem().toString());
            stat.setString(10, kewarganegaraan);
            stat.setString(11, txt_nama_negara.getText());
            stat.setString(12, jComboBerkubutuhan.getSelectedItem().toString());
            stat.setString(13, txt_alamat_jalan.getText());
            stat.setString(14, txt_rt.getText());
            stat.setString(15, txt_rw.getText());
            stat.setString(16, txt_nama_dusun.getText());
            stat.setString(17, txt_desa.getText());
            stat.setString(18, txt_kecamatan.getText());
            stat.setString(19, txt_kode_pos.getText());
            stat.setString(20, txt_lintang.getText());
            stat.setString(21, txt_bujur.getText());
            stat.setString(22, jComboTempatTinggal.getSelectedItem().toString());
            stat.setString(23, jComboModaTransportasi.getSelectedItem().toString());
            stat.setString(24, txt_nomor_kks.getText());
            stat.setString(25, txt_anak_ke.getText());
            stat.setString(26, penerima_kps);
            stat.setString(27, txt_no_kps.getText());
            stat.setString(28, usulan_dari_sekolah);
            stat.setString(29, penerima_kip);
            stat.setString(30, txt_nomor_kip.getText());
            stat.setString(31, txt_nama_kip.getText());
            stat.setString(32, kartu_kip);
            stat.setString(33, jComboPip.getSelectedItem().toString());
            stat.setString(34, txt_bank.getText());
            stat.setString(35, txt_nomor_rekening.getText());
            stat.setString(36, txt_nama_rekening.getText());

            stat.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data berhasil di simpan");
            autonumber();
            kosong();

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Data gagal di simpan" + e);
        }
    }

    private void ubahData() {
        if (r_laki.isSelected()) {
            gender = "Laki-laki";
        } else if (r_perempuan.isSelected()) {
            gender = "Perempuan";
        } else {
            JOptionPane.showMessageDialog(null, "Jenis Kelamin tidak boleh kosong");
        }
        if (r_wni.isSelected()) {
            kewarganegaraan = "WNI";
        } else if (r_wna.isSelected()) {
            kewarganegaraan = "WNA";
        } else {
            JOptionPane.showMessageDialog(null, "Kewarganegaraan tidak boleh kosong");
        }
        if (r_ya_kps.isSelected()) {
            penerima_kps = "Ya";
        } else if (r_tidak_kps.isSelected()) {
            penerima_kps = "Tidak";
        } else {
            JOptionPane.showMessageDialog(null, "Penerima KPS tidak boleh kosong");
        }
        if (r_ya_pip.isSelected()) {
            usulan_dari_sekolah = "Ya";
        } else if (r_tidak_pip.isSelected()) {
            usulan_dari_sekolah = "Tidak";
        } else {
            JOptionPane.showMessageDialog(null, "Usulan dari sekolah tidak boleh kosong");
        }
        if (r_ya_kip.isSelected()) {
            penerima_kip = "Ya";
        } else if (r_tidak_kip.isSelected()) {
            penerima_kip = "Tidak";
        } else {
            JOptionPane.showMessageDialog(null, "Penerima KIP tidak boleh kosong");
        }
        if (r_ya_kartu.isSelected()) {
            kartu_kip = "Ya";
        } else if (r_tidak_kartu.isSelected()) {
            kartu_kip = "Tidak";
        } else {
            JOptionPane.showMessageDialog(null, "Kartu KIP tidak boleh kosong");
        }
        String query = "UPDATE `data_pribadi_siswa` SET `no_registrasi`=?,`nama_lengkap`=?,"
                + "`jenis_kelamin`=?,`nisn`=?,`nik`=?,`tempat_lahir`=?,`tanggal_lahir`=?,`nomor_akta_lahir`=?,`agama`=?,"
                + "`kewarganegaraan`=?,`nama_negara`=?,`berkebutuhan_khusus`=?,`alamat_jalan`=?,`rt`=?,`rw`=?,`nama_dusun`=?,"
                + "`nama_desa`=?,`kecamatan`=?,`kode_pos`=?,`lintang`=?,`bujur`=?,`tempat_tinggal`=?,`moda_transportasi`=?,"
                + "`nomor_kks`=?,`anak_keberapa`=?,`penerima_kps`=?,`nomor_kps`=?,`usulan_dari_sekolah`=?,`penerima_kip`=?,"
                + "`nomor_kip`=?,`nama_kip`=?,`kartu_kip`=?,`alasan_layak_pip`=?,`bank`=?,`nomor_rekening`=?,`nama_rekening`=?"
                + " WHERE no_registrasi='" + txt_registrasi.getText() + "'";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String tanggal = String.valueOf(dateFormat.format(jDateChooser.getDate()));
        try {
            PreparedStatement stat = MyConnection.getConnection().prepareStatement(query);
            stat.setString(1, txt_registrasi.getText());
            stat.setString(2, txt_nama_siswa.getText());
            stat.setString(3, gender);
            stat.setString(4, txt_nisn.getText());
            stat.setString(5, txt_nik.getText());
            stat.setString(6, txt_tempat_lahir.getText());
            stat.setString(7, tanggal);
            stat.setString(8, txt_no_regist_akta.getText());
            stat.setString(9, jComboAgama.getSelectedItem().toString());
            stat.setString(10, kewarganegaraan);
            stat.setString(11, txt_nama_negara.getText());
            stat.setString(12, jComboBerkubutuhan.getSelectedItem().toString());
            stat.setString(13, txt_alamat_jalan.getText());
            stat.setString(14, txt_rt.getText());
            stat.setString(15, txt_rw.getText());
            stat.setString(16, txt_nama_dusun.getText());
            stat.setString(17, txt_desa.getText());
            stat.setString(18, txt_kecamatan.getText());
            stat.setString(19, txt_kode_pos.getText());
            stat.setString(20, txt_lintang.getText());
            stat.setString(21, txt_bujur.getText());
            stat.setString(22, jComboTempatTinggal.getSelectedItem().toString());
            stat.setString(23, jComboModaTransportasi.getSelectedItem().toString());
            stat.setString(24, txt_nomor_kks.getText());
            stat.setString(25, txt_anak_ke.getText());
            stat.setString(26, penerima_kps);
            stat.setString(27, txt_no_kps.getText());
            stat.setString(28, usulan_dari_sekolah);
            stat.setString(29, penerima_kip);
            stat.setString(30, txt_nomor_kip.getText());
            stat.setString(31, txt_nama_kip.getText());
            stat.setString(32, kartu_kip);
            stat.setString(33, jComboPip.getSelectedItem().toString());
            stat.setString(34, txt_bank.getText());
            stat.setString(35, txt_nomor_rekening.getText());
            stat.setString(36, txt_nama_rekening.getText());

            stat.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data berhasil di ubah");
//            autonumber();

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Data gagal di ubah" + e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();
        buttonGroup4 = new javax.swing.ButtonGroup();
        buttonGroup5 = new javax.swing.ButtonGroup();
        buttonGroup6 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        txt_registrasi = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        txt_nama_siswa = new javax.swing.JTextField();
        txt_nisn = new javax.swing.JTextField();
        r_laki = new javax.swing.JRadioButton();
        r_perempuan = new javax.swing.JRadioButton();
        txt_nik = new javax.swing.JTextField();
        txt_tempat_lahir = new javax.swing.JTextField();
        txt_nomor_kip = new javax.swing.JTextField();
        txt_nomor_kks = new javax.swing.JTextField();
        txt_alamat_jalan = new javax.swing.JTextField();
        txt_rt = new javax.swing.JTextField();
        txt_rw = new javax.swing.JTextField();
        txt_nama_dusun = new javax.swing.JTextField();
        txt_desa = new javax.swing.JTextField();
        txt_kecamatan = new javax.swing.JTextField();
        txt_kode_pos = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        txt_lintang = new javax.swing.JTextField();
        txt_bujur = new javax.swing.JTextField();
        txt_anak_ke = new javax.swing.JTextField();
        jComboAgama = new javax.swing.JComboBox<>();
        jComboBerkubutuhan = new javax.swing.JComboBox<>();
        jComboTempatTinggal = new javax.swing.JComboBox<>();
        jComboModaTransportasi = new javax.swing.JComboBox<>();
        txt_no_kps = new javax.swing.JTextField();
        txt_nama_kip = new javax.swing.JTextField();
        r_ya_kps = new javax.swing.JRadioButton();
        r_tidak_kps = new javax.swing.JRadioButton();
        r_ya_pip = new javax.swing.JRadioButton();
        r_tidak_pip = new javax.swing.JRadioButton();
        r_ya_kip = new javax.swing.JRadioButton();
        r_tidak_kip = new javax.swing.JRadioButton();
        r_ya_kartu = new javax.swing.JRadioButton();
        r_tidak_kartu = new javax.swing.JRadioButton();
        jComboPip = new javax.swing.JComboBox<>();
        txt_bank = new javax.swing.JTextField();
        txt_nomor_rekening = new javax.swing.JTextField();
        txt_nama_rekening = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        txt_no_regist_akta = new javax.swing.JTextField();
        txt_nama_negara = new javax.swing.JTextField();
        btn_simpan = new javax.swing.JButton();
        btn_batal = new javax.swing.JButton();
        jDateChooser = new com.toedter.calendar.JDateChooser();
        r_wni = new javax.swing.JRadioButton();
        r_wna = new javax.swing.JRadioButton();
        btn_ubahData = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel2.setText("Nama Lengkap");

        jLabel3.setText("Jenis Kelamin");

        jLabel4.setText("NISN");

        jLabel5.setText("NIK/ No. KITAS (Untuk WNA)");

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Data Pribadi");

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel21.setText("No. Registrasi : ");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel21)
                .addGap(18, 18, 18)
                .addComponent(txt_registrasi, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
                    .addComponent(txt_registrasi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jLabel6.setText("Tempat Lahir");

        jLabel7.setText("Tanggal Lahir");

        jLabel8.setText("No Registasi Akta Lahir");

        jLabel9.setText("Agama & Kepercayaan");

        jLabel10.setText("Kewarganegaraan");

        jLabel11.setText("Nama Negara");

        jLabel12.setText("Berkebutuhan Khusus");

        jLabel13.setText("Alamat Jalan");

        jLabel14.setText("RT");

        jLabel15.setText("RW");

        jLabel16.setText("Nama Dusun");

        jLabel17.setText("Nama Kelurahan/ Desa");

        jLabel18.setText("Kecamatan");

        jLabel19.setText("Kode Pos");

        r_laki.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(r_laki);
        r_laki.setText("Laki-laki");
        r_laki.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        r_perempuan.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(r_perempuan);
        r_perempuan.setText("Perempuan");
        r_perempuan.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jLabel20.setText("Lintang ");

        jLabel22.setText("Tempat Tinggal ");

        jLabel23.setText("Moda Transportasi ");

        jLabel24.setText("Nomor KKS (Kartu Keluarga Sejahtera)  ");

        jLabel25.setText("Anak keberapa");

        jLabel26.setText("Penerima KPS/PKH ");

        jLabel27.setText("No. KPS/PKH (apabila menerima) ");

        jLabel28.setText("Usulan Dari Sekolah (Layak PIP) ");

        jLabel29.setText("Penerima KIP (Kartu Indonesia Pintar) ");

        jLabel30.setText("Nomor KIP ");

        jLabel31.setText("Nama tertera di KIP ");

        jLabel32.setText("Terima fisik Kartu (KIP) ");

        jLabel33.setText("Alasan layak PIP");

        jLabel34.setText("Bank ");

        jLabel35.setText("No rekening bank ");

        jLabel36.setText("Rekening atas nama ");

        jComboAgama.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Islam", "Kristen/ Protestan", "Katholik", "Hindu", "Budha", "Khonghucu", "Lainnya" }));

        jComboBerkubutuhan.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tidak", "Netra (A)", "Rungu (B)", "Grahita ringan (C)", "Grahita Sedang (C1)", "Daksa Ringan (D)", "Daksa Sedang (D1)", "Laras (E)", "Wicara (F)", " Tuna ganda (G)", "Hiper aktif (H)", "Cerdas Istimewa (i)", "Bakat Istimewa (J)", "Kesulitan Belajra (K)", "Narkoba (N)", "Indigo (O)", "Down Sindrome (P)", "Autis (Q)" }));

        jComboTempatTinggal.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Bersama orang tua", "Wali", "Kos", "Asrama", "Panti Asuhan", "Lainnya" }));

        jComboModaTransportasi.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Jalan kaki", "Kendaraan pribadi", "Kendaraan Umum/angkot/Pete-pete", "Jemputan Sekolah", "Kereta Api", "Ojek", "Andong/Bendi/Sado/ Dokar/Delman/Beca", "Perahu penyebrangan/Rakit/Getek", "Lainnya" }));

        r_ya_kps.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup3.add(r_ya_kps);
        r_ya_kps.setText("Ya");

        r_tidak_kps.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup3.add(r_tidak_kps);
        r_tidak_kps.setText("Tidak");

        r_ya_pip.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup4.add(r_ya_pip);
        r_ya_pip.setText("Ya");

        r_tidak_pip.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup4.add(r_tidak_pip);
        r_tidak_pip.setText("Tidak");

        r_ya_kip.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup5.add(r_ya_kip);
        r_ya_kip.setText("Ya");

        r_tidak_kip.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup5.add(r_tidak_kip);
        r_tidak_kip.setText("Tidak");

        r_ya_kartu.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup6.add(r_ya_kartu);
        r_ya_kartu.setText("Ya");

        r_tidak_kartu.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup6.add(r_tidak_kartu);
        r_tidak_kartu.setText("Tidak");

        jComboPip.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pemegang PKH/KPS/KIP", "Penerima BSM 2014", "Yatim Piatu/Panti Asuhan/Panti Sosial", "Dampak Bencana Alam", "Pernah Drop OUT", "Siswa Miskin/Rentan Miskin", "Daerah Konflik", "Keluarga Terpidana", "Kelainan Fisik" }));

        jLabel37.setText("Bujur");

        btn_simpan.setBackground(new java.awt.Color(153, 255, 255));
        btn_simpan.setText("Simpan");
        btn_simpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_simpanActionPerformed(evt);
            }
        });

        btn_batal.setBackground(new java.awt.Color(255, 102, 102));
        btn_batal.setForeground(new java.awt.Color(255, 255, 255));
        btn_batal.setText("Batal");
        btn_batal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_batalActionPerformed(evt);
            }
        });

        r_wni.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup2.add(r_wni);
        r_wni.setText("WNI");
        r_wni.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        r_wna.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup2.add(r_wna);
        r_wna.setText("WNA");
        r_wna.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        btn_ubahData.setBackground(new java.awt.Color(0, 102, 204));
        btn_ubahData.setForeground(new java.awt.Color(255, 255, 255));
        btn_ubahData.setText("Ubah");
        btn_ubahData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ubahDataActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel15, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(txt_nisn)
                        .addComponent(txt_kode_pos)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(r_laki)
                            .addGap(18, 18, 18)
                            .addComponent(r_perempuan, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(txt_nama_siswa)
                        .addComponent(txt_nik)
                        .addComponent(txt_tempat_lahir)
                        .addComponent(jComboAgama, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txt_no_regist_akta)
                        .addComponent(jComboBerkubutuhan, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txt_alamat_jalan)
                        .addComponent(txt_rt)
                        .addComponent(txt_rw)
                        .addComponent(txt_nama_dusun)
                        .addComponent(txt_desa)
                        .addComponent(txt_kecamatan)
                        .addComponent(txt_nama_negara)
                        .addComponent(jDateChooser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(r_wni)
                        .addGap(18, 18, 18)
                        .addComponent(r_wna)))
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel36, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel35, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel34, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel33, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel32, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel31, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel30, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel29, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel28, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel27, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel26, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel37, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btn_batal, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)
                        .addComponent(btn_ubahData)
                        .addGap(8, 8, 8)
                        .addComponent(btn_simpan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(txt_lintang)
                    .addComponent(txt_bujur)
                    .addComponent(txt_anak_ke)
                    .addComponent(txt_nomor_kip)
                    .addComponent(txt_nomor_kks)
                    .addComponent(jComboTempatTinggal, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jComboModaTransportasi, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txt_no_kps)
                    .addComponent(txt_nama_kip)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(r_ya_kps)
                        .addGap(18, 18, 18)
                        .addComponent(r_tidak_kps))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(r_ya_pip)
                        .addGap(18, 18, 18)
                        .addComponent(r_tidak_pip))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(r_ya_kip)
                        .addGap(18, 18, 18)
                        .addComponent(r_tidak_kip))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(r_ya_kartu)
                        .addGap(18, 18, 18)
                        .addComponent(r_tidak_kartu))
                    .addComponent(jComboPip, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txt_bank)
                    .addComponent(txt_nomor_rekening)
                    .addComponent(txt_nama_rekening))
                .addGap(32, 32, 32))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txt_nama_siswa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20)
                    .addComponent(txt_lintang, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(r_laki)
                    .addComponent(r_perempuan)
                    .addComponent(txt_bujur, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel37))
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txt_nisn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel22)
                    .addComponent(jComboTempatTinggal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txt_nik, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel23)
                    .addComponent(jComboModaTransportasi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txt_tempat_lahir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel24)
                    .addComponent(txt_nomor_kks, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel7)
                        .addComponent(jLabel25)
                        .addComponent(txt_anak_ke, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jLabel26)
                    .addComponent(r_ya_kps)
                    .addComponent(r_tidak_kps)
                    .addComponent(txt_no_regist_akta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel27)
                    .addComponent(jComboAgama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_no_kps, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jLabel28)
                    .addComponent(r_ya_pip)
                    .addComponent(r_tidak_pip)
                    .addComponent(r_wni)
                    .addComponent(r_wna))
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(jLabel29)
                    .addComponent(r_ya_kip)
                    .addComponent(r_tidak_kip)
                    .addComponent(txt_nama_negara, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jLabel30)
                    .addComponent(txt_nomor_kip, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBerkubutuhan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(txt_alamat_jalan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel31)
                    .addComponent(txt_nama_kip, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(txt_rt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel32)
                    .addComponent(r_ya_kartu)
                    .addComponent(r_tidak_kartu))
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(txt_rw, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel33)
                    .addComponent(jComboPip, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(txt_nama_dusun, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel34)
                    .addComponent(txt_bank, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(txt_desa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel35)
                    .addComponent(txt_nomor_rekening, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(txt_kecamatan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel36)
                    .addComponent(txt_nama_rekening, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(txt_kode_pos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_simpan)
                    .addComponent(btn_batal)
                    .addComponent(btn_ubahData))
                .addContainerGap(21, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 855, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 554, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_simpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_simpanActionPerformed
        // TODO add your handling code here:
        id = txt_registrasi.getText();
        Date tanggal = jDateChooser.getDate();
        String nama = txt_nama_siswa.getText();
        String nisn = txt_nisn.getText();
        String nik = txt_nik.getText();
        String tempat_lahir = txt_tempat_lahir.getText();
        String no_regist_akta = txt_no_regist_akta.getText();
        String agama = jComboAgama.getSelectedItem().toString();
        String negara = txt_nama_negara.getText();
        String Berkubutuhan = jComboBerkubutuhan.getSelectedItem().toString();
        String alamat = txt_alamat_jalan.getText();
        String rt = txt_rt.getText();
        String rw = txt_rw.getText();
        String dusun = txt_nama_dusun.getText();
        String desa = txt_desa.getText();
        String kecamatan = txt_kecamatan.getText();
        String kode_pos = txt_kode_pos.getText();
        String lintang = txt_lintang.getText();
        String bujur = txt_bujur.getText();
        String tempat_tinggal = jComboTempatTinggal.getSelectedItem().toString();
        String moda = jComboModaTransportasi.getSelectedItem().toString();
        String kks = txt_nomor_kks.getText();
        String anak_ke = txt_anak_ke.getText();
        String kps = txt_no_kps.getText();
        String kip = txt_nomor_kip.getText();
        String nama_kip = txt_nama_kip.getText();
        String bank = txt_bank.getText();
        String nomor_rek = txt_nomor_rekening.getText();
        String nama_rek = txt_nama_rekening.getText();
        if (nama.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Nama tidak boleh kosong!");
            txt_nama_siswa.requestFocus();
        } else if (nisn.isEmpty()) {
            JOptionPane.showMessageDialog(null, "NISN tidak boleh kosong!");
            txt_nisn.requestFocus();
        } else if (nik.isEmpty()) {
            JOptionPane.showMessageDialog(null, "NIK/ No. KITAS tidak boleh kosong!");
            txt_nik.requestFocus();
        } else if (tempat_lahir.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Tempat lahir tidak boleh kosong!");
            txt_tempat_lahir.requestFocus();
        } else if (tanggal == null) {
            JOptionPane.showMessageDialog(null, "Tanggal tidak boleh kosong!");
            jDateChooser.requestFocus();
        } else if (no_regist_akta.isEmpty()) {
            JOptionPane.showMessageDialog(null, "No Registasi Akta Lahir tidak boleh kosong!");
            txt_no_regist_akta.requestFocus();
        } else if (agama == null) {
            JOptionPane.showMessageDialog(null, "Agama tidak boleh kosong!");
        } else if (negara.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Nama negara tidak boleh kosong!");
            txt_nama_negara.requestFocus();
        } else if (Berkubutuhan == null) {
            JOptionPane.showMessageDialog(null, "Berkebutuhan Khusus tidak boleh kosong!");
        } else if (alamat.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Alamat jalan tidak boleh kosong!");
            txt_alamat_jalan.requestFocus();
        } else if (rt.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Rt tidak boleh kosong!");
            txt_rt.requestFocus();
        } else if (rw.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Rw tidak boleh kosong!");
            txt_rw.requestFocus();
        } else if (dusun.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Dusun tidak boleh kosong!");
            txt_nama_dusun.requestFocus();
        } else if (desa.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Desa tidak boleh kosong!");
            txt_desa.requestFocus();
        } else if (kecamatan.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Kecamatan tidak boleh kosong!");
            txt_kecamatan.requestFocus();
        } else if (kode_pos.isEmpty()) {
            JOptionPane.showMessageDialog(null, "kode pos tidak boleh kosong!");
            txt_kode_pos.requestFocus();
        } else if (tempat_tinggal == null) {
            JOptionPane.showMessageDialog(null, "Tempat tinggal tidak boleh kosong!");
        } else if (moda == null) {
            JOptionPane.showMessageDialog(null, "Moda Transportasi tidak boleh kosong!");
        } else if (anak_ke.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Anak keberapa tidak boleh kosong!");
            txt_anak_ke.requestFocus();
        } else if (bank.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Bank tidak boleh kosong!");
            txt_bank.requestFocus();
        } else if (nomor_rek.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Nomor rekening tidak boleh kosong!");
            txt_nomor_rekening.requestFocus();
        } else if (nama_rek.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Rekening atas nama tidak boleh kosong!");
            txt_nama_rekening.requestFocus();
        } else {
            if (!checkUser(id)) {
                simpanData();
            } else {
                JOptionPane.showMessageDialog(null, "ID siswa sudah ada!");
            }
        }
    }//GEN-LAST:event_btn_simpanActionPerformed

    private void btn_batalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_batalActionPerformed
        // TODO add your handling code here:
        kosong();
        this.dispose();
    }//GEN-LAST:event_btn_batalActionPerformed

    private void btn_ubahDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ubahDataActionPerformed
        // TODO add your handling code here:
        String nama = txt_nama_siswa.getText();
        Date tanggal = jDateChooser.getDate();
        String nisn = txt_nisn.getText();
        String nik = txt_nik.getText();
        String tempat_lahir = txt_tempat_lahir.getText();
        String no_regist_akta = txt_no_regist_akta.getText();
        String agama = jComboAgama.getSelectedItem().toString();
        String negara = txt_nama_negara.getText();
        String Berkubutuhan = jComboBerkubutuhan.getSelectedItem().toString();
        String alamat = txt_alamat_jalan.getText();
        String rt = txt_rt.getText();
        String rw = txt_rw.getText();
        String dusun = txt_nama_dusun.getText();
        String desa = txt_desa.getText();
        String kecamatan = txt_kecamatan.getText();
        String kode_pos = txt_kode_pos.getText();
        String lintang = txt_lintang.getText();
        String bujur = txt_bujur.getText();
        String tempat_tinggal = jComboTempatTinggal.getSelectedItem().toString();
        String moda = jComboModaTransportasi.getSelectedItem().toString();
        String kks = txt_nomor_kks.getText();
        String anak_ke = txt_anak_ke.getText();
        String kps = txt_no_kps.getText();
        String kip = txt_nomor_kip.getText();
        String nama_kip = txt_nama_kip.getText();
        String bank = txt_bank.getText();
        String nomor_rek = txt_nomor_rekening.getText();
        String nama_rek = txt_nama_rekening.getText();
        if (nama.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Nama tidak boleh kosong!");
            txt_nama_siswa.requestFocus();
        } else if (nisn.isEmpty()) {
            JOptionPane.showMessageDialog(null, "NISN tidak boleh kosong!");
            txt_nisn.requestFocus();
        } else if (nik.isEmpty()) {
            JOptionPane.showMessageDialog(null, "NIK/ No. KITAS tidak boleh kosong!");
            txt_nik.requestFocus();
        } else if (tempat_lahir.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Tempat lahir tidak boleh kosong!");
            txt_tempat_lahir.requestFocus();
        } else if (tanggal == null) {
            JOptionPane.showMessageDialog(null, "Tanggal tidak boleh kosong!");
            jDateChooser.requestFocus();
        } else if (no_regist_akta.isEmpty()) {
            JOptionPane.showMessageDialog(null, "No Registasi Akta Lahir tidak boleh kosong!");
            txt_no_regist_akta.requestFocus();
        } else if (agama == null) {
            JOptionPane.showMessageDialog(null, "Agama tidak boleh kosong!");
        } else if (negara.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Nama negara tidak boleh kosong!");
            txt_nama_negara.requestFocus();
        } else if (Berkubutuhan == null) {
            JOptionPane.showMessageDialog(null, "Berkebutuhan Khusus tidak boleh kosong!");
        } else if (alamat.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Alamat jalan tidak boleh kosong!");
            txt_alamat_jalan.requestFocus();
        } else if (rt.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Rt tidak boleh kosong!");
            txt_rt.requestFocus();
        } else if (rw.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Rw tidak boleh kosong!");
            txt_rw.requestFocus();
        } else if (dusun.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Dusun tidak boleh kosong!");
            txt_nama_dusun.requestFocus();
        } else if (desa.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Desa tidak boleh kosong!");
            txt_desa.requestFocus();
        } else if (kecamatan.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Kecamatan tidak boleh kosong!");
            txt_kecamatan.requestFocus();
        } else if (kode_pos.isEmpty()) {
            JOptionPane.showMessageDialog(null, "kode pos tidak boleh kosong!");
            txt_kode_pos.requestFocus();
        } else if (tempat_tinggal == null) {
            JOptionPane.showMessageDialog(null, "Tempat tinggal tidak boleh kosong!");
        } else if (moda == null) {
            JOptionPane.showMessageDialog(null, "Moda Transportasi tidak boleh kosong!");
        } else if (anak_ke.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Anak keberapa tidak boleh kosong!");
            txt_anak_ke.requestFocus();
        } else if (bank.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Bank tidak boleh kosong!");
            txt_bank.requestFocus();
        } else if (nomor_rek.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Nomor rekening tidak boleh kosong!");
            txt_nomor_rekening.requestFocus();
        } else if (nama_rek.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Rekening atas nama tidak boleh kosong!");
            txt_nama_rekening.requestFocus();
        } else {
            ubahData();
            kosong();
            this.dispose();
        }
    }//GEN-LAST:event_btn_ubahDataActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormDataPribadi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormDataPribadi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormDataPribadi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormDataPribadi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FormDataPribadi().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_batal;
    public javax.swing.JButton btn_simpan;
    public javax.swing.JButton btn_ubahData;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.ButtonGroup buttonGroup4;
    private javax.swing.ButtonGroup buttonGroup5;
    private javax.swing.ButtonGroup buttonGroup6;
    public javax.swing.JComboBox<String> jComboAgama;
    public javax.swing.JComboBox<String> jComboBerkubutuhan;
    public javax.swing.JComboBox<String> jComboModaTransportasi;
    public javax.swing.JComboBox<String> jComboPip;
    public javax.swing.JComboBox<String> jComboTempatTinggal;
    public com.toedter.calendar.JDateChooser jDateChooser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    public javax.swing.JRadioButton r_laki;
    public javax.swing.JRadioButton r_perempuan;
    public javax.swing.JRadioButton r_tidak_kartu;
    public javax.swing.JRadioButton r_tidak_kip;
    public javax.swing.JRadioButton r_tidak_kps;
    public javax.swing.JRadioButton r_tidak_pip;
    public javax.swing.JRadioButton r_wna;
    public javax.swing.JRadioButton r_wni;
    public javax.swing.JRadioButton r_ya_kartu;
    public javax.swing.JRadioButton r_ya_kip;
    public javax.swing.JRadioButton r_ya_kps;
    public javax.swing.JRadioButton r_ya_pip;
    public javax.swing.JTextField txt_alamat_jalan;
    public javax.swing.JTextField txt_anak_ke;
    public javax.swing.JTextField txt_bank;
    public javax.swing.JTextField txt_bujur;
    public javax.swing.JTextField txt_desa;
    public javax.swing.JTextField txt_kecamatan;
    public javax.swing.JTextField txt_kode_pos;
    public javax.swing.JTextField txt_lintang;
    public javax.swing.JTextField txt_nama_dusun;
    public javax.swing.JTextField txt_nama_kip;
    public javax.swing.JTextField txt_nama_negara;
    public javax.swing.JTextField txt_nama_rekening;
    public javax.swing.JTextField txt_nama_siswa;
    public javax.swing.JTextField txt_nik;
    public javax.swing.JTextField txt_nisn;
    public javax.swing.JTextField txt_no_kps;
    public javax.swing.JTextField txt_no_regist_akta;
    public javax.swing.JTextField txt_nomor_kip;
    public javax.swing.JTextField txt_nomor_kks;
    public javax.swing.JTextField txt_nomor_rekening;
    public javax.swing.JLabel txt_registrasi;
    public javax.swing.JTextField txt_rt;
    public javax.swing.JTextField txt_rw;
    public javax.swing.JTextField txt_tempat_lahir;
    // End of variables declaration//GEN-END:variables
}
