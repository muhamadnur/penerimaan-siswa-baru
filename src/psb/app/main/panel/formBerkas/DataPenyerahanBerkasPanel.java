/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psb.app.main.panel.formBerkas;

import java.awt.Container;
import java.sql.Connection;
import psb.app.main.panel.pendaftaran.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import koneksi.MyConnection;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.swing.JRViewer;
import net.sf.jasperreports.view.JasperViewer;
import psb.app.main.form.TableDataSiswaPemPanel;
import psb.app.main.form.TableDataSiswaPenyerahanBerkas;
import session.UserSession;

/**
 *
 * @author Admin
 */
public class DataPenyerahanBerkasPanel extends javax.swing.JPanel {

    public String id_siswa;
    public String namaSiswa;
    private DefaultTableModel tabmode;
    String user_id = UserSession.getId_registrasi();
    String nama_user = UserSession.getNama();
    DefaultTableCellRenderer renderer;
    PreparedStatement ps;
    ResultSet rs;

    /**
     * Creates new form PenyerahanBerkasPanel
     */
    public DataPenyerahanBerkasPanel() {
        initComponents();
        datatable();
        kosong();
        aktif();
    }

    private void aktif() {
        r_tidak_formulir.setSelected(true);
        r_tidak_formulir.setSelected(true);
        r_tidak_ijazah.setSelected(true);
        r_tidak_skhun.setSelected(true);
        r_tidak_raport.setSelected(true);
        r_tidak_akta.setSelected(true);
        r_tidak_kk.setSelected(true);
        r_tidak_ktp.setSelected(true);
        r_tidak_kis.setSelected(true);
        r_tidak_foto.setSelected(true);
        r_tidak_lain.setSelected(true);
        r_tidak_asal.setSelected(true);
        r_tidak_dapotik.setSelected(true);
        r_tidak_raportAsli.setSelected(true);
        r_tidak_nisn.setSelected(true);
        r_tidak_sertifikat.setSelected(true);
        radio_tidak_lain.setSelected(true);
    }

    public void itemTerpilih() {
        TableDataSiswaPenyerahanBerkas Dpw = new TableDataSiswaPenyerahanBerkas();
        Dpw.berkas = this;
        txt_idSiswa.setText(id_siswa);
        txt_namaSiswa.setText(namaSiswa);
    }

    private void datatable() {
        String[] Header = {"No.", "ID Siswa", "Nama Siswa", "Gelombang", "ID Penerima Berkas", "Nama Penerima Berkas", "Waktu Penyerahan Berkas"};
        tabmode = new DefaultTableModel(Header, 0);
        renderer = (DefaultTableCellRenderer) table_berkas.getTableHeader().getDefaultRenderer();
        renderer.setHorizontalAlignment(JLabel.LEFT);
        String cariitem = txt_cari.getText();
        PreparedStatement ps;
        try {
            String query = "SELECT `siswa_id`, `nama_siswa`, `gelombang`, `id_penerima`, `nama_penerima`, `created_at` FROM `form_berkas` WHERE `siswa_id` like '%" + cariitem + "%' or `nama_siswa` like '%" + cariitem + "%' order by `siswa_id` desc";
            ps = MyConnection.getConnection().prepareStatement(query);
            ResultSet rs = ps.executeQuery(query);
            int no = 1;
            while (rs.next()) {
                tabmode.addRow(new Object[]{
                    Integer.toString(no),
                    rs.getString(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getString(4),
                    rs.getString(5),
                    rs.getString(6)
                });

                no++;
            }
            table_berkas.setModel(tabmode);
            table_berkas.getColumnModel().getColumn(0).setPreferredWidth(10);
            table_berkas.getColumnModel().getColumn(1).setPreferredWidth(100);
            table_berkas.getColumnModel().getColumn(2).setPreferredWidth(200);
            table_berkas.getColumnModel().getColumn(3).setPreferredWidth(100);
            table_berkas.getColumnModel().getColumn(4).setPreferredWidth(100);
            table_berkas.getColumnModel().getColumn(5).setPreferredWidth(200);
            table_berkas.getColumnModel().getColumn(6).setPreferredWidth(200);
            table_berkas.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "data gagal dipanggil" + e);
        }
    }

    private void kosong() {
        txt_idSiswa.setText("");
        txt_namaSiswa.setText("");
        jComboGelombang.setSelectedItem(null);
    }

    private void saveData() {
        String idSiswa = txt_idSiswa.getText().trim();
        String namaSiswa = txt_namaSiswa.getText().trim();
        String gelombang = jComboGelombang.getSelectedItem().toString();

        String query = "INSERT INTO `form_berkas`( `siswa_id`, `nama_siswa`, `gelombang`, `id_penerima`, `nama_penerima`) VALUES (?,?,?,?,?)";
        try {
            PreparedStatement stat = MyConnection.getConnection().prepareStatement(query);
            stat.setString(1, idSiswa);
            stat.setString(2, namaSiswa);
            stat.setString(3, gelombang);
            stat.setString(4, user_id);
            stat.setString(5, nama_user);
            stat.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data berhasil di simpan");
            printData();
            kosong();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Data gagal di simpan" + e);
        }
    }
    private void printData() {
        try {
            String namaFile = "src\\report\\dataPenyerahanBerkas.jasper";
            HashMap param = new HashMap();
            param.put("siswaID", txt_idSiswa.getText());  // siswaID  is ireport parameter name
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void saveBerkas() {
        String formulir = null;
        if (r_ya_formulir.isSelected()) {
            formulir = "Ya";
        } else {
            formulir = "Tidak";
        }
        String ijazah = null;
        if (r_ya_ijazah.isSelected()) {
            ijazah = "Ya";
        } else {
            ijazah = "Tidak";
        }
        String skhun = null;
        if (r_ya_skhun.isSelected()) {
            skhun = "Ya";
        } else {
            skhun = "Tidak";
        }
        String raport = null;
        if (r_ya_raport.isSelected()) {
            raport = "Ya";
        } else {
            raport = "Tidak";
        }
        String akta = null;
        if (r_ya_akta.isSelected()) {
            akta = "Ya";
        } else {
            akta = "Tidak";
        }
        String kk = null;
        if (r_ya_kk.isSelected()) {
            kk = "Ya";
        } else {
            kk = "Tidak";
        }
        String ktp = null;
        if (r_ya_ktp.isSelected()) {
            ktp = "Ya";
        } else {
            ktp = "Tidak";
        }
        String kis = null;
        if (r_ya_kis.isSelected()) {
            kis = "Ya";
        } else {
            kis = "Tidak";
        }
        String foto = null;
        if (r_ya_foto.isSelected()) {
            foto = "Ya";
        } else {
            foto = "Tidak";
        }
        String lain = null;
        if (r_ya_lain.isSelected()) {
            lain = "Ya";
        } else {
            lain = "Tidak";
        }
        String id_siswa = txt_idSiswa.getText();
        String nama = txt_namaSiswa.getText();
        String gelombang = jComboGelombang.getSelectedItem().toString();
        String query = "INSERT INTO `penyerahan_berkas`(`siswa_id`, `nama_siswa`, `gelombang`, `formulir`, `ijazah`, `skhun`, `raport`, `akta`,"
                + " `kk`, `ktp`, `kis`, `pas_foto`, `lain_lain`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            ps = MyConnection.getConnection().prepareStatement(query);
            ps.setString(1, id_siswa);
            ps.setString(2, nama);
            ps.setString(3, gelombang);
            ps.setString(4, formulir);
            ps.setString(5, ijazah);
            ps.setString(6, skhun);
            ps.setString(7, raport);
            ps.setString(8, akta);
            ps.setString(9, kk);
            ps.setString(10, ktp);
            ps.setString(11, kis);
            ps.setString(12, foto);
            ps.setString(13, lain);
            ps.executeUpdate();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Data gagal di simpan" + e);
        }
    }

    private void persyaratan() {
        String asal = null;
        if (r_ya_asal.isSelected()) {
            asal = "Ya";
        } else {
            asal = "Tidak";
        }
        String dapotik = null;
        if (r_ya_dapotik.isSelected()) {
            dapotik = "Ya";
        } else {
            dapotik = "Tidak";
        }
        String raportAsli = null;
        if (r_ya_raportAsli.isSelected()) {
            raportAsli = "Ya";
        } else {
            raportAsli = "Tidak";
        }
        String nisn = null;
        if (r_ya_nisn.isSelected()) {
            nisn = "Ya";
        } else {
            nisn = "Tidak";
        }
        String sertifikat = null;
        if (r_ya_sertifikat.isSelected()) {
            sertifikat = "Ya";
        } else {
            sertifikat = "Tidak";
        }
        String lainPersyaratan = null;
        if (radio_y_lain.isSelected()) {
            lainPersyaratan = "Ya";
        } else {
            lainPersyaratan = "Tidak";
        }
        String query = "INSERT INTO `persyaratan_tambahan`(`siswa_id`, `nama_siswa`, `gelombang`, `surat_pindah_sekolah`, `surat_dapotik`, "
                + "`raport_asli`, `validasi_nisn`, `sertifikat`, `lain_lain`) VALUES (?,?,?,?,?,?,?,?,?)";
        try {
            ps = MyConnection.getConnection().prepareStatement(query);
            ps.setString(1, txt_idSiswa.getText());
            ps.setString(2, txt_namaSiswa.getText());
            ps.setString(3, jComboGelombang.getSelectedItem().toString());
            ps.setString(4, asal);
            ps.setString(5, dapotik);
            ps.setString(6, raportAsli);
            ps.setString(7, nisn);
            ps.setString(8, sertifikat);
            ps.setString(9, lainPersyaratan);
            ps.executeUpdate();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Data gagal di simpan" + e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();
        buttonGroup4 = new javax.swing.ButtonGroup();
        buttonGroup5 = new javax.swing.ButtonGroup();
        buttonGroup6 = new javax.swing.ButtonGroup();
        buttonGroup7 = new javax.swing.ButtonGroup();
        buttonGroup8 = new javax.swing.ButtonGroup();
        buttonGroup9 = new javax.swing.ButtonGroup();
        buttonGroup10 = new javax.swing.ButtonGroup();
        buttonGroup11 = new javax.swing.ButtonGroup();
        buttonGroup12 = new javax.swing.ButtonGroup();
        buttonGroup13 = new javax.swing.ButtonGroup();
        buttonGroup14 = new javax.swing.ButtonGroup();
        buttonGroup15 = new javax.swing.ButtonGroup();
        buttonGroup16 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txt_idSiswa = new javax.swing.JTextField();
        txt_namaSiswa = new javax.swing.JTextField();
        jComboGelombang = new javax.swing.JComboBox<>();
        jButton2 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        r_ya_asal = new javax.swing.JRadioButton();
        r_tidak_asal = new javax.swing.JRadioButton();
        r_ya_nisn = new javax.swing.JRadioButton();
        r_tidak_nisn = new javax.swing.JRadioButton();
        r_ya_dapotik = new javax.swing.JRadioButton();
        r_tidak_dapotik = new javax.swing.JRadioButton();
        r_ya_sertifikat = new javax.swing.JRadioButton();
        r_tidak_sertifikat = new javax.swing.JRadioButton();
        r_ya_raportAsli = new javax.swing.JRadioButton();
        r_tidak_raportAsli = new javax.swing.JRadioButton();
        radio_y_lain = new javax.swing.JRadioButton();
        radio_tidak_lain = new javax.swing.JRadioButton();
        jPanel5 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        r_ya_formulir = new javax.swing.JRadioButton();
        r_tidak_formulir = new javax.swing.JRadioButton();
        r_ya_ijazah = new javax.swing.JRadioButton();
        r_ya_akta = new javax.swing.JRadioButton();
        r_tidak_akta = new javax.swing.JRadioButton();
        r_ya_foto = new javax.swing.JRadioButton();
        r_tidak_foto = new javax.swing.JRadioButton();
        r_tidak_ijazah = new javax.swing.JRadioButton();
        r_ya_kk = new javax.swing.JRadioButton();
        r_tidak_kk = new javax.swing.JRadioButton();
        r_ya_lain = new javax.swing.JRadioButton();
        r_tidak_lain = new javax.swing.JRadioButton();
        r_ya_skhun = new javax.swing.JRadioButton();
        r_tidak_skhun = new javax.swing.JRadioButton();
        r_ya_ktp = new javax.swing.JRadioButton();
        r_ya_raport = new javax.swing.JRadioButton();
        r_tidak_raport = new javax.swing.JRadioButton();
        r_ya_kis = new javax.swing.JRadioButton();
        r_tidak_kis = new javax.swing.JRadioButton();
        r_tidak_ktp = new javax.swing.JRadioButton();
        btn_hapus = new javax.swing.JButton();
        btn_edit = new javax.swing.JButton();
        btn_simpan = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        table_berkas = new javax.swing.JTable();
        txt_cari = new javax.swing.JTextField();
        btn_cari = new javax.swing.JButton();

        setLayout(new java.awt.BorderLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setPreferredSize(new java.awt.Dimension(793, 38));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Form Penyerahan Berkas");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel2.setText("ID Siswa");

        jLabel3.setText("Nama Siswa");

        jLabel4.setText("Gelombang");

        jComboGelombang.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Gelombang 1", "Gelombang 2", "Gelombang 3" }));

        jButton2.setText("Cari");
        jButton2.setMaximumSize(new java.awt.Dimension(51, 20));
        jButton2.setMinimumSize(new java.awt.Dimension(51, 20));
        jButton2.setPreferredSize(new java.awt.Dimension(51, 20));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txt_namaSiswa)
                    .addComponent(jComboGelombang, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(txt_idSiswa, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)
                        .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, 69, Short.MAX_VALUE)))
                .addContainerGap(54, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txt_idSiswa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txt_namaSiswa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jComboGelombang, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Tambahan Persyaratan Untuk Siswa Pindahan");

        jLabel17.setText("Surat Pindah Dari Sekolah Asal");

        jLabel18.setText("Surat Pindah Dapotik");

        jLabel19.setText("Raport Asli Dari Sekolah Asal");

        jLabel20.setText("Validasi NISN");

        jLabel21.setText("Fotocopy Sertifikat Akreditasi Sekolah");

        jLabel22.setText("Lain-lain");

        r_ya_asal.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup11.add(r_ya_asal);
        r_ya_asal.setText("Ya");

        r_tidak_asal.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup11.add(r_tidak_asal);
        r_tidak_asal.setText("Tidak");

        r_ya_nisn.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup14.add(r_ya_nisn);
        r_ya_nisn.setText("Ya");

        r_tidak_nisn.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup14.add(r_tidak_nisn);
        r_tidak_nisn.setText("Tidak");

        r_ya_dapotik.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup12.add(r_ya_dapotik);
        r_ya_dapotik.setText("Ya");

        r_tidak_dapotik.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup12.add(r_tidak_dapotik);
        r_tidak_dapotik.setText("Tidak");

        r_ya_sertifikat.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup15.add(r_ya_sertifikat);
        r_ya_sertifikat.setText("Ya");

        r_tidak_sertifikat.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup15.add(r_tidak_sertifikat);
        r_tidak_sertifikat.setText("Tidak");

        r_ya_raportAsli.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup13.add(r_ya_raportAsli);
        r_ya_raportAsli.setText("Ya");

        r_tidak_raportAsli.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup13.add(r_tidak_raportAsli);
        r_tidak_raportAsli.setText("Tidak");

        radio_y_lain.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup16.add(radio_y_lain);
        radio_y_lain.setText("Ya");

        radio_tidak_lain.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup16.add(radio_tidak_lain);
        radio_tidak_lain.setText("Tidak");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel17)
                            .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(8, 8, 8)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(r_ya_asal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(r_ya_dapotik, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(r_ya_raportAsli, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(r_tidak_raportAsli, javax.swing.GroupLayout.DEFAULT_SIZE, 69, Short.MAX_VALUE)
                            .addComponent(r_tidak_dapotik, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(r_tidak_asal, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(8, 8, 8)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(r_ya_nisn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(r_ya_sertifikat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(radio_y_lain, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(radio_tidak_lain, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
                            .addComponent(r_tidak_sertifikat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(r_tidak_nisn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(20, 20, 20))))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jLabel6)
                .addGap(8, 8, 8)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(jLabel20)
                    .addComponent(r_ya_asal)
                    .addComponent(r_tidak_asal)
                    .addComponent(r_ya_nisn)
                    .addComponent(r_tidak_nisn))
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(jLabel21)
                    .addComponent(r_ya_dapotik)
                    .addComponent(r_tidak_dapotik)
                    .addComponent(r_ya_sertifikat)
                    .addComponent(r_tidak_sertifikat))
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(r_ya_raportAsli)
                        .addComponent(r_tidak_raportAsli))
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(radio_y_lain)
                        .addComponent(radio_tidak_lain))
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel19)
                        .addComponent(jLabel22)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Penyerahan Berkas");

        jLabel7.setText("Formulir Sudah Diisi Lengkap");

        jLabel8.setText("Fotocopy Ijazah SMP/MTS");

        jLabel9.setText("Fotocopy SKHUN");

        jLabel10.setText("Fotocopy Raport");

        jLabel11.setText("Fotocopy Akta Kelahiran");

        jLabel12.setText("Fotocopy Kartu Keluarga");

        jLabel13.setText("Fotocopy KTP (Ayah atau Ibu)");

        jLabel14.setText("Fotocopy KIS");

        jLabel15.setText("Pas Foto 3x4");

        jLabel16.setText("Lain-lain");

        r_ya_formulir.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(r_ya_formulir);
        r_ya_formulir.setText("Ya");

        r_tidak_formulir.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(r_tidak_formulir);
        r_tidak_formulir.setText("Tidak");

        r_ya_ijazah.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup2.add(r_ya_ijazah);
        r_ya_ijazah.setText("Ya");
        r_ya_ijazah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_ya_ijazahActionPerformed(evt);
            }
        });

        r_ya_akta.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup5.add(r_ya_akta);
        r_ya_akta.setText("Ya");

        r_tidak_akta.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup5.add(r_tidak_akta);
        r_tidak_akta.setText("Tidak");

        r_ya_foto.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup9.add(r_ya_foto);
        r_ya_foto.setText("Ya");

        r_tidak_foto.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup9.add(r_tidak_foto);
        r_tidak_foto.setText("Tidak");

        r_tidak_ijazah.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup2.add(r_tidak_ijazah);
        r_tidak_ijazah.setText("Tidak");

        r_ya_kk.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup6.add(r_ya_kk);
        r_ya_kk.setText("Ya");

        r_tidak_kk.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup6.add(r_tidak_kk);
        r_tidak_kk.setText("Tidak");

        r_ya_lain.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup10.add(r_ya_lain);
        r_ya_lain.setText("Ya");

        r_tidak_lain.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup10.add(r_tidak_lain);
        r_tidak_lain.setText("Tidak");

        r_ya_skhun.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup3.add(r_ya_skhun);
        r_ya_skhun.setText("Ya");
        r_ya_skhun.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_ya_skhunActionPerformed(evt);
            }
        });

        r_tidak_skhun.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup3.add(r_tidak_skhun);
        r_tidak_skhun.setText("Tidak");

        r_ya_ktp.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup7.add(r_ya_ktp);
        r_ya_ktp.setText("Ya");

        r_ya_raport.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup4.add(r_ya_raport);
        r_ya_raport.setText("Ya");
        r_ya_raport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_ya_raportActionPerformed(evt);
            }
        });

        r_tidak_raport.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup4.add(r_tidak_raport);
        r_tidak_raport.setText("Tidak");

        r_ya_kis.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup8.add(r_ya_kis);
        r_ya_kis.setText("Ya");

        r_tidak_kis.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup8.add(r_tidak_kis);
        r_tidak_kis.setText("Tidak");

        r_tidak_ktp.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup7.add(r_tidak_ktp);
        r_tidak_ktp.setText("Tidak");

        btn_hapus.setBackground(new java.awt.Color(242, 38, 19));
        btn_hapus.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_hapus.setForeground(new java.awt.Color(255, 255, 255));
        btn_hapus.setText("Hapus");
        btn_hapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_hapusActionPerformed(evt);
            }
        });

        btn_edit.setBackground(new java.awt.Color(255, 255, 51));
        btn_edit.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_edit.setText("Edit");
        btn_edit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_editActionPerformed(evt);
            }
        });

        btn_simpan.setBackground(new java.awt.Color(34, 167, 240));
        btn_simpan.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_simpan.setForeground(new java.awt.Color(255, 255, 255));
        btn_simpan.setText("Simpan");
        btn_simpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_simpanActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(722, 722, 722))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btn_hapus)
                        .addGap(10, 10, 10)
                        .addComponent(btn_edit)
                        .addGap(10, 10, 10)
                        .addComponent(btn_simpan))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(8, 8, 8)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(r_ya_formulir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(r_ya_ijazah, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(r_ya_skhun, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(r_ya_raport, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(r_tidak_ijazah, javax.swing.GroupLayout.DEFAULT_SIZE, 53, Short.MAX_VALUE)
                            .addComponent(r_tidak_formulir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(r_tidak_skhun, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(r_tidak_raport, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 47, Short.MAX_VALUE)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE))
                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(r_ya_akta)
                            .addComponent(r_ya_kk)
                            .addComponent(r_ya_ktp)
                            .addComponent(r_ya_kis))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(r_tidak_ktp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(r_tidak_akta, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(r_tidak_kk, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(r_tidak_kis, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 150, Short.MAX_VALUE)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
                            .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(8, 8, 8)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(r_ya_foto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(r_ya_lain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(r_tidak_foto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(r_tidak_lain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(20, 20, 20))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jLabel5)
                .addGap(6, 6, 6)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel11)
                    .addComponent(jLabel15)
                    .addComponent(r_ya_formulir)
                    .addComponent(r_tidak_formulir)
                    .addComponent(r_ya_akta)
                    .addComponent(r_tidak_akta)
                    .addComponent(r_ya_foto)
                    .addComponent(r_tidak_foto))
                .addGap(6, 6, 6)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jLabel12)
                    .addComponent(jLabel16)
                    .addComponent(r_ya_ijazah)
                    .addComponent(r_tidak_ijazah)
                    .addComponent(r_ya_kk)
                    .addComponent(r_tidak_kk)
                    .addComponent(r_ya_lain)
                    .addComponent(r_tidak_lain))
                .addGap(6, 6, 6)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(r_ya_skhun)
                    .addComponent(r_tidak_skhun)
                    .addComponent(r_ya_ktp)
                    .addComponent(r_tidak_ktp))
                .addGap(6, 6, 6)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jLabel14)
                    .addComponent(r_ya_raport)
                    .addComponent(r_tidak_raport)
                    .addComponent(r_ya_kis)
                    .addComponent(r_tidak_kis))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_simpan)
                    .addComponent(btn_hapus)
                    .addComponent(btn_edit))
                .addGap(20, 20, 20))
        );

        table_berkas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", ""
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        table_berkas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                table_berkasMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(table_berkas);

        txt_cari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_cariKeyReleased(evt);
            }
        });

        btn_cari.setText("Cari");
        btn_cari.setMaximumSize(new java.awt.Dimension(51, 20));
        btn_cari.setMinimumSize(new java.awt.Dimension(51, 20));
        btn_cari.setPreferredSize(new java.awt.Dimension(51, 20));
        btn_cari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cariActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 939, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(txt_cari, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_cari, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(10, 10, 10))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_cari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_cari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        add(jPanel1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void btn_hapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_hapusActionPerformed
        // TODO add your handling code here:
        int ok = JOptionPane.showConfirmDialog(null, "hapus", "konfirmasi dialog", JOptionPane.YES_NO_OPTION);
        if (ok == 0) {
            int index = table_berkas.getSelectedRow();
            DefaultTableModel model = (DefaultTableModel) table_berkas.getModel();
            String reg_number = model.getValueAt(index, 1).toString();
            deleteBerkas();
            deletePersyaratan();
            try {
                String query = "DELETE FROM `form_berkas` WHERE siswa_id='" + reg_number + "'";
                ps = MyConnection.getConnection().prepareStatement(query);
                ps.executeUpdate();
                JOptionPane.showMessageDialog(null, "data berhasil dihapus");
                kosong();
                aktif();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "data gagal dihapus" + e);
            }
            datatable();
        }
    }//GEN-LAST:event_btn_hapusActionPerformed
    private void deleteBerkas() {
        try {
            String query = "DELETE FROM `penyerahan_berkas` WHERE siswa_id='" + txt_idSiswa.getText() + "'";
            ps = MyConnection.getConnection().prepareStatement(query);
            ps.executeUpdate();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "data gagal dihapus" + e);
        }
    }

    private void deletePersyaratan() {
        try {
            String query = "DELETE FROM `persyaratan_tambahan` WHERE siswa_id='" + txt_idSiswa.getText() + "'";
            ps = MyConnection.getConnection().prepareStatement(query);
            ps.executeUpdate();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "data gagal dihapus" + e);
        }
    }
    private void btn_simpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_simpanActionPerformed
        // TODO add your handling code here:
        String idString = txt_idSiswa.getText();
        String nama = txt_namaSiswa.getText();
        String gelombang = jComboGelombang.getSelectedItem().toString();
        if (idString.isEmpty()) {
            JOptionPane.showMessageDialog(null, "ID siswa tidak boleh kosong");
        } else if (nama.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Nama Siswa tidak boleh kosong");
        } else if (gelombang == null) {
            JOptionPane.showMessageDialog(null, "Gelombang tidak boleh kosong");
        } else {
            saveBerkas();
            persyaratan();
            saveData();
            datatable();
            kosong();
            aktif();
        }

    }//GEN-LAST:event_btn_simpanActionPerformed

    private void txt_cariKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_cariKeyReleased
        // TODO add your handling code here:
        datatable();
    }//GEN-LAST:event_txt_cariKeyReleased

    private void btn_cariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cariActionPerformed
        // TODO add your handling code here:
        datatable();
    }//GEN-LAST:event_btn_cariActionPerformed

    private void table_berkasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table_berkasMouseClicked
        // TODO add your handling code here:
        int r = table_berkas.getSelectedRow();

        String id_siswa = table_berkas.getValueAt(r, 1).toString();
        String nama_siswa = table_berkas.getValueAt(r, 2).toString();
        String gelombang = table_berkas.getValueAt(r, 3).toString();

        txt_idSiswa.setText(id_siswa);
        txt_namaSiswa.setText(nama_siswa);
        jComboGelombang.setSelectedItem(gelombang);
        selectedDataBerkas();
        selectedDataPersyaratan();

    }//GEN-LAST:event_table_berkasMouseClicked
    private void selectedDataBerkas() {
        String sql = "SELECT `formulir`, `ijazah`, `skhun`, `raport`, `akta`, `kk`, `ktp`, `kis`, `pas_foto`, `lain_lain` FROM `penyerahan_berkas` "
                + "WHERE siswa_id='" + txt_idSiswa.getText() + "'";
        try {
            ps = MyConnection.getConnection().prepareStatement(sql);
            rs = ps.executeQuery(sql);
            while (rs.next()) {
                String lain_lain = rs.getString("lain_lain");
                if (lain_lain.equals("Ya")) {
                    r_ya_lain.setSelected(true);
                } else {
                    r_tidak_lain.setSelected(true);
                }
                String pas_foto = rs.getString("pas_foto");
                if (pas_foto.equals("Ya")) {
                    r_ya_foto.setSelected(true);
                } else {
                    r_tidak_foto.setSelected(true);
                }
                String kis = rs.getString("kis");
                if (kis.equals("Ya")) {
                    r_ya_kis.setSelected(true);
                } else {
                    r_tidak_kis.setSelected(true);
                }
                String ktp = rs.getString("ktp");
                if (ktp.equals("Ya")) {
                    r_ya_ktp.setSelected(true);
                } else {
                    r_tidak_ktp.setSelected(true);
                }
                String kk = rs.getString("kk");
                if (kk.equals("Ya")) {
                    r_ya_kk.setSelected(true);
                } else {
                    r_tidak_kk.setSelected(true);
                }
                String akta = rs.getString("akta");
                if (akta.equals("Ya")) {
                    r_ya_akta.setSelected(true);
                } else {
                    r_tidak_akta.setSelected(true);
                }
                String raport = rs.getString("raport");
                if (raport.equals("Ya")) {
                    r_ya_raport.setSelected(true);
                } else {
                    r_tidak_raport.setSelected(true);
                }
                String formulir = rs.getString("formulir");
                if (formulir.equals("Ya")) {
                    r_ya_formulir.setSelected(true);
                } else {
                    r_tidak_formulir.setSelected(true);
                }
                String ijazah = rs.getString("ijazah");
                if (ijazah.equals("Ya")) {
                    r_ya_ijazah.setSelected(true);
                } else {
                    r_tidak_ijazah.setSelected(true);
                }
                String skhun = rs.getString("skhun");
                if (skhun.equals("Ya")) {
                    r_ya_skhun.setSelected(true);
                } else {
                    r_tidak_skhun.setSelected(true);
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "data gagal panggil" + e);
        }

    }

    private void selectedDataPersyaratan() {
        String sql = "SELECT `surat_pindah_sekolah`, `surat_dapotik`, `raport_asli`, `validasi_nisn`, `sertifikat`, `lain_lain` FROM `persyaratan_tambahan` "
                + "WHERE siswa_id='" + txt_idSiswa.getText() + "'";
        try {
            ps = MyConnection.getConnection().prepareStatement(sql);
            rs = ps.executeQuery(sql);
            while (rs.next()) {
                String radioLain = rs.getString("lain_lain");
                if (radioLain.equals("Ya")) {
                    radio_y_lain.setSelected(true);
                } else {
                    radio_tidak_lain.setSelected(true);
                }
                String sertifikat = rs.getString("sertifikat");
                if (sertifikat.equals("Ya")) {
                    r_ya_sertifikat.setSelected(true);
                } else {
                    r_tidak_sertifikat.setSelected(true);
                }
                String nisn = rs.getString("validasi_nisn");
                if (nisn.equals("Ya")) {
                    r_ya_nisn.setSelected(true);
                } else {
                    r_tidak_nisn.setSelected(true);
                }
                String raportAsli = rs.getString("raport_asli");
                if (raportAsli.equals("Ya")) {
                    r_ya_raportAsli.setSelected(true);
                } else {
                    r_tidak_raportAsli.setSelected(true);
                }
                String asal = rs.getString("surat_pindah_sekolah");
                if (asal.equals("Ya")) {
                    r_ya_asal.setSelected(true);
                } else {
                    r_tidak_asal.setSelected(true);
                }
                String surat_dapotik = rs.getString("surat_dapotik");
                if (surat_dapotik.equals("Ya")) {
                    r_ya_dapotik.setSelected(true);
                } else {
                    r_tidak_dapotik.setSelected(true);
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "data gagal panggil" + e);
        }
    }
    private void btn_editActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_editActionPerformed
        // TODO add your handling code here:
        int ok = JOptionPane.showConfirmDialog(null, "Edit", "konfirmasi dialog", JOptionPane.YES_NO_OPTION);
        if (ok == 0) {
            String id_siswa = txt_idSiswa.getText().trim();
            String namaSiswa = txt_namaSiswa.getText().trim();
            String gelombang = jComboGelombang.getSelectedItem().toString();

            String query = "UPDATE `form_berkas` SET `siswa_id`=?,`nama_siswa`=?,`gelombang`=? WHERE `siswa_id`='" + id_siswa + "'";
            try {
                PreparedStatement stat = MyConnection.getConnection().prepareStatement(query);
                stat.setString(1, id_siswa);
                stat.setString(2, namaSiswa);
                stat.setString(3, gelombang);
                stat.executeUpdate();
                JOptionPane.showMessageDialog(null, "Data berhasil di ubah");
                updateBerkas();
                updatePersyaratan();
                kosong();
                aktif();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Data gagal di ubah" + e);
            }
            datatable();
        }

    }//GEN-LAST:event_btn_editActionPerformed
    private void updateBerkas() {
        String formulir = null;
        if (r_ya_formulir.isSelected()) {
            formulir = "Ya";
        } else {
            formulir = "Tidak";
        }
        String ijazah = null;
        if (r_ya_ijazah.isSelected()) {
            ijazah = "Ya";
        } else {
            ijazah = "Tidak";
        }
        String skhun = null;
        if (r_ya_skhun.isSelected()) {
            skhun = "Ya";
        } else {
            skhun = "Tidak";
        }
        String raport = null;
        if (r_ya_raport.isSelected()) {
            raport = "Ya";
        } else {
            raport = "Tidak";
        }
        String akta = null;
        if (r_ya_akta.isSelected()) {
            akta = "Ya";
        } else {
            akta = "Tidak";
        }
        String kk = null;
        if (r_ya_kk.isSelected()) {
            kk = "Ya";
        } else {
            kk = "Tidak";
        }
        String ktp = null;
        if (r_ya_ktp.isSelected()) {
            ktp = "Ya";
        } else {
            ktp = "Tidak";
        }
        String kis = null;
        if (r_ya_kis.isSelected()) {
            kis = "Ya";
        } else {
            kis = "Tidak";
        }
        String foto = null;
        if (r_ya_foto.isSelected()) {
            foto = "Ya";
        } else {
            foto = "Tidak";
        }
        String lain = null;
        if (r_ya_lain.isSelected()) {
            lain = "Ya";
        } else {
            lain = "Tidak";
        }
        String id_siswa = txt_idSiswa.getText();
        String nama = txt_namaSiswa.getText();
        String gelombang = jComboGelombang.getSelectedItem().toString();
        String query = "UPDATE `penyerahan_berkas` SET `siswa_id`=?,`nama_siswa`=?,`gelombang`=?,`formulir`=?,`ijazah`=?,`skhun`=?,`raport`=?,"
                + "`akta`=?,`kk`=?,`ktp`=?,`kis`=?,`pas_foto`=?,`lain_lain`=? WHERE siswa_id='" + id_siswa + "'";
        try {
            ps = MyConnection.getConnection().prepareStatement(query);
            ps.setString(1, id_siswa);
            ps.setString(2, nama);
            ps.setString(3, gelombang);
            ps.setString(4, formulir);
            ps.setString(5, ijazah);
            ps.setString(6, skhun);
            ps.setString(7, raport);
            ps.setString(8, akta);
            ps.setString(9, kk);
            ps.setString(10, ktp);
            ps.setString(11, kis);
            ps.setString(12, foto);
            ps.setString(13, lain);
            ps.executeUpdate();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Data gagal di ubah" + e);
        }
    }

    private void updatePersyaratan() {
        String asal = null;
        if (r_ya_asal.isSelected()) {
            asal = "Ya";
        } else {
            asal = "Tidak";
        }
        String dapotik = null;
        if (r_ya_dapotik.isSelected()) {
            dapotik = "Ya";
        } else {
            dapotik = "Tidak";
        }
        String raportAsli = null;
        if (r_ya_raportAsli.isSelected()) {
            raportAsli = "Ya";
        } else {
            raportAsli = "Tidak";
        }
        String nisn = null;
        if (r_ya_nisn.isSelected()) {
            nisn = "Ya";
        } else {
            nisn = "Tidak";
        }
        String sertifikat = null;
        if (r_ya_sertifikat.isSelected()) {
            sertifikat = "Ya";
        } else {
            sertifikat = "Tidak";
        }
        String lainPersyaratan = null;
        if (radio_y_lain.isSelected()) {
            lainPersyaratan = "Ya";
        } else {
            lainPersyaratan = "Tidak";
        }
        String query = "UPDATE `persyaratan_tambahan` SET `siswa_id`=?,`nama_siswa`=?,`gelombang`=?,`surat_pindah_sekolah`=?,`surat_dapotik`=?,"
                + "`raport_asli`=?,`validasi_nisn`=?,`sertifikat`=?,`lain_lain`=? WHERE siswa_id='" + txt_idSiswa.getText() + "'";
        try {
            ps = MyConnection.getConnection().prepareStatement(query);
            ps.setString(1, txt_idSiswa.getText());
            ps.setString(2, txt_namaSiswa.getText());
            ps.setString(3, jComboGelombang.getSelectedItem().toString());
            ps.setString(4, asal);
            ps.setString(5, dapotik);
            ps.setString(6, raportAsli);
            ps.setString(7, nisn);
            ps.setString(8, sertifikat);
            ps.setString(9, lainPersyaratan);
            ps.executeUpdate();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Data gagal di ubah" + e);
        }
    }
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        TableDataSiswaPenyerahanBerkas Dpw = new TableDataSiswaPenyerahanBerkas();
        Dpw.berkas = this;
        Dpw.pack();
        Dpw.setVisible(true);
        Dpw.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void r_ya_ijazahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_ya_ijazahActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_r_ya_ijazahActionPerformed

    private void r_ya_skhunActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_ya_skhunActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_r_ya_skhunActionPerformed

    private void r_ya_raportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_ya_raportActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_r_ya_raportActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cari;
    private javax.swing.JButton btn_edit;
    private javax.swing.JButton btn_hapus;
    private javax.swing.JButton btn_simpan;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup10;
    private javax.swing.ButtonGroup buttonGroup11;
    private javax.swing.ButtonGroup buttonGroup12;
    private javax.swing.ButtonGroup buttonGroup13;
    private javax.swing.ButtonGroup buttonGroup14;
    private javax.swing.ButtonGroup buttonGroup15;
    private javax.swing.ButtonGroup buttonGroup16;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.ButtonGroup buttonGroup4;
    private javax.swing.ButtonGroup buttonGroup5;
    private javax.swing.ButtonGroup buttonGroup6;
    private javax.swing.ButtonGroup buttonGroup7;
    private javax.swing.ButtonGroup buttonGroup8;
    private javax.swing.ButtonGroup buttonGroup9;
    private javax.swing.JButton jButton2;
    private javax.swing.JComboBox<String> jComboGelombang;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JRadioButton r_tidak_akta;
    private javax.swing.JRadioButton r_tidak_asal;
    private javax.swing.JRadioButton r_tidak_dapotik;
    private javax.swing.JRadioButton r_tidak_formulir;
    private javax.swing.JRadioButton r_tidak_foto;
    private javax.swing.JRadioButton r_tidak_ijazah;
    private javax.swing.JRadioButton r_tidak_kis;
    private javax.swing.JRadioButton r_tidak_kk;
    private javax.swing.JRadioButton r_tidak_ktp;
    private javax.swing.JRadioButton r_tidak_lain;
    private javax.swing.JRadioButton r_tidak_nisn;
    private javax.swing.JRadioButton r_tidak_raport;
    private javax.swing.JRadioButton r_tidak_raportAsli;
    private javax.swing.JRadioButton r_tidak_sertifikat;
    private javax.swing.JRadioButton r_tidak_skhun;
    private javax.swing.JRadioButton r_ya_akta;
    private javax.swing.JRadioButton r_ya_asal;
    private javax.swing.JRadioButton r_ya_dapotik;
    private javax.swing.JRadioButton r_ya_formulir;
    private javax.swing.JRadioButton r_ya_foto;
    private javax.swing.JRadioButton r_ya_ijazah;
    private javax.swing.JRadioButton r_ya_kis;
    private javax.swing.JRadioButton r_ya_kk;
    private javax.swing.JRadioButton r_ya_ktp;
    private javax.swing.JRadioButton r_ya_lain;
    private javax.swing.JRadioButton r_ya_nisn;
    private javax.swing.JRadioButton r_ya_raport;
    private javax.swing.JRadioButton r_ya_raportAsli;
    private javax.swing.JRadioButton r_ya_sertifikat;
    private javax.swing.JRadioButton r_ya_skhun;
    private javax.swing.JRadioButton radio_tidak_lain;
    private javax.swing.JRadioButton radio_y_lain;
    private javax.swing.JTable table_berkas;
    private javax.swing.JTextField txt_cari;
    private javax.swing.JTextField txt_idSiswa;
    private javax.swing.JTextField txt_namaSiswa;
    // End of variables declaration//GEN-END:variables
}
