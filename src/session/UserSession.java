/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

/**
 *
 * @author Admin
 */
public class UserSession {
//    membuat variabel session
    private static String id_registrasi;
    private static String nama;
    private static int role_id;
    private static String username;
//    mendapatkan session
    public static String getId_registrasi(){
        return id_registrasi;
    }
    public static void setId_registrasi(String id_registrasi){
        UserSession.id_registrasi=id_registrasi;
    }
    public static String getNama(){
        return nama;
    }
    public static void setNama(String nama){
        UserSession.nama=nama;
    }
    public static int getRole_id(){
        return role_id;
    }
    public static void setRole_id(int role_id){
        UserSession.role_id=role_id;
    }

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        UserSession.username = username;
    }
  
}
